﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProCIF.DataPribadi.API.Models
{
    public class GlobalVariabelList
    {
        private string _ConnectionStringCIF;
        private string _ConnectionStringCFMAST;
        private string _ConnectionStringLOG;

        public GlobalVariabelList()
        {
            _ConnectionStringCIF = "";
            _ConnectionStringCFMAST = "";
            _ConnectionStringLOG = "";
        }

        public string ConnectionStringCIF { get => _ConnectionStringCIF; set => _ConnectionStringCIF = value; }
        public string ConnectionStringCFMAST { get => _ConnectionStringCFMAST; set => _ConnectionStringCFMAST = value; }
        public string ConnectionStringLOG { get => _ConnectionStringLOG; set => _ConnectionStringLOG = value; }
    }
}
