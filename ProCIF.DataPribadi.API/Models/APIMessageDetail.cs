﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProCIF.DataPribadi.API.Models
{
    public class APIMessageDetail<ModelResult>
    {
        private bool _IsSuccess;
        private bool _IsNeedApproval;
        private string _Description;
        private ModelResult _Data;

        public APIMessageDetail()
        {
            _IsSuccess = false;
            _IsNeedApproval = false;
            _Description = "";
        }
        public bool IsSuccess { get => _IsSuccess; set => _IsSuccess = value; }
        public bool IsNeedApproval { get => _IsNeedApproval; set => _IsNeedApproval = value; }
        public string Description { get => _Description; set => _Description = value; }
        public ModelResult Data { get => _Data; set => _Data = value; }
    }
}
