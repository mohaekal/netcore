﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCIF.DataPribadi.API.Services
{
    public class clsWebServiceCIF : IDisposable
    {
        private string _strGUID;
        private string _strUserNIK;
        private string _strSubSystem;
        private int _intUserNIK;
        private StringBuilder _strXMLSend;

        wsProCIF.clsServiceSoapClient soapClient = new wsProCIF.clsServiceSoapClient(wsProCIF.clsServiceSoapClient.EndpointConfiguration.clsServiceSoap);

        public clsWebServiceCIF(string strNIK, string strGUID, string strModule, string urlEndpoint)
        {
            this._strUserNIK = strNIK;
            this._strGUID = strGUID;
            this._strSubSystem = strModule;
            this._intUserNIK = int.Parse(strNIK);
            soapClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(urlEndpoint);
        }

        public bool CallCIFAddQuickCIFCreation01700(string strRqDetail, out string strResult, out string strError)
        {
            int intErrorNumber = 0;
            string strRejectDesc = "";
            strResult = "";
            strError = "";

            ServiceHeader();//replace hasil header
            this._strXMLSend.Replace("[SERVICENAME]", "CIF_Add_QuickCIFCreation_01700");
            this._strXMLSend.Replace("[OPERATIONCODE]", "100");
            this._strXMLSend.Replace("[REQUESTDETAILFIELD]", strRqDetail);
            this._strXMLSend.Replace("[MOREINDICATOR]", "N");
            bool blnResult;
            blnResult = false;
            try
            {
                wsProCIF.CIFAddQuickCIFCreation01700Response response = soapClient.CIFAddQuickCIFCreation01700Async(this._strGUID, this._strXMLSend.ToString(), this._strUserNIK).Result;
                strResult = response.Body.XMLResult;
                intErrorNumber = response.Body.ErrorNumber;
                strRejectDesc = response.Body.RejectDesc;
                strResult = strResult.Replace("ns1:", "");
                blnResult = true;
            }

            catch (Exception ex)
            {
                strError = ex.Message;
                blnResult = false;
            }

            if (strRejectDesc != "")
            {
                if (strRejectDesc.Trim().Equals("Duplikasi No. ID dan Tipe (bisa Overwrite)") ||
                    strRejectDesc.Trim().Equals("Nomor ID di black-list (bisa di overwrite)") ||
                    strRejectDesc.Trim().Equals("ID di high risk customer (bisa di overwrite)"))
                {
                    blnResult = false;
                    strResult = strRejectDesc.Trim();
                }
                else
                {
                    blnResult = false;
                    strError = "Error# " + intErrorNumber.ToString() + " " + strRejectDesc;
                }
            }
            return blnResult;
        }

        public bool CallCIFChangeCIFBasicInformationV2MaintPROCIF01822(string strRqDetail, out string strResult, out string strError)
        {
            int intErrorNumber = 0;
            string strRejectDesc = "";
            strResult = "";
            strError = "";

            ServiceHeader();//replace hasil header
            this._strXMLSend.Replace("[SERVICENAME]", "CIF_Change_CIFBasicInformationV2MaintPROCIF_01822");
            this._strXMLSend.Replace("[OPERATIONCODE]", "010");
            this._strXMLSend.Replace("[REQUESTDETAILFIELD]", strRqDetail);
            this._strXMLSend.Replace("[MOREINDICATOR]", "N");
            
            bool blnResult;
            blnResult = false;
            try
            {
                wsProCIF.CIFChangeCIFBasicInformationV2MaintPROCIF01822Response response = soapClient.CIFChangeCIFBasicInformationV2MaintPROCIF01822Async(this._strGUID, this._strXMLSend.ToString(), this._strUserNIK).Result;
                strResult = response.Body.XMLResult;
                intErrorNumber = response.Body.ErrorNumber;
                strRejectDesc = response.Body.RejectDesc;
                strResult = strResult.Replace("ns1:", "");
                blnResult = true;
            }

            catch (Exception ex)
            {
                strError = ex.Message;
                blnResult = false;
            }

            if (strRejectDesc != "")
            {
                blnResult = false;
                strError = "Error# " + intErrorNumber.ToString() + " " + strRejectDesc;
            }
            return blnResult;
        }

        public bool CallCIFInquiryCIFDetail01610(string strRqDetail, out string strResult, out string strError)
        {
            int intErrorNumber = 0;
            string strRejectDesc = "";
            strResult = "";
            strError = "";

            ServiceHeader();//replace hasil header
            this._strXMLSend.Replace("[SERVICENAME]", "CIF_Inquiry_CIFDetail_01610");
            this._strXMLSend.Replace("[OPERATIONCODE]", "000");
            this._strXMLSend.Replace("[REQUESTDETAILFIELD]", strRqDetail.ToString());
            this._strXMLSend.Replace("[MOREINDICATOR]", "N");
            bool blnResult = false;

            try
            {
                wsProCIF.CIFInquiryCIFDetail01610Response response = soapClient.CIFInquiryCIFDetail01610Async(this._strGUID, this._strXMLSend.ToString(), this._strUserNIK).Result;
                strResult = response.Body.XMLResult;
                intErrorNumber = response.Body.ErrorNumber;
                strRejectDesc = response.Body.RejectDesc;
                strResult = strResult.Replace("ns1:", "");
                blnResult = true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                blnResult = false;
            }

            if (strRejectDesc != "")
            {
                if (strRejectDesc.Trim() == "Data tidak ada di Database.(MBFCF01610)")
                {
                    blnResult = true;
                }
                else
                {
                    blnResult = false;
                    strRejectDesc = "Error# " + intErrorNumber.ToString() + " " + strRejectDesc;
                }
                strError = strRejectDesc.Trim();
            }
            return blnResult;
        }

        public bool CallCIFInquiryBasicInfoDetails01600(string strRqDetail, out string strResult, out string strError)
        {
            int intErrorNumber = 0;
            string strRejectDesc = "";
            strResult = "";
            strError = "";

            ServiceHeader();//replace hasil header
            this._strXMLSend.Replace("[SERVICENAME]", "CIF_Inquiry_BasicInfoDetails_01600");
            this._strXMLSend.Replace("[OPERATIONCODE]", "000");
            this._strXMLSend.Replace("[REQUESTDETAILFIELD]", strRqDetail.ToString());
            this._strXMLSend.Replace("[MOREINDICATOR]", "N");
            bool blnResult = false;

            try
            {
                wsProCIF.CIFInquiryBasicInfoDetails01600Response response = soapClient.CIFInquiryBasicInfoDetails01600Async(this._strGUID, this._strXMLSend.ToString(), this._strUserNIK).Result;
                strResult = response.Body.XMLResult;
                intErrorNumber = response.Body.ErrorNumber;
                strRejectDesc = response.Body.RejectDesc;
                strResult = strResult.Replace("ns1:", "");
                blnResult = true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                blnResult = false;
            }

            if (strRejectDesc != "")
            {
                if (strRejectDesc.Trim() == "Data tidak ada di Database.(MBFCF01600)")
                {
                    blnResult = true;
                }
                else
                {
                    blnResult = false;
                    strRejectDesc = "Error# " + intErrorNumber.ToString() + " " + strRejectDesc;
                }
                strError = strRejectDesc.Trim();
            }
            return blnResult;
        }

        public bool CallCIFInquiryCIFSearchfromCreditCard01697(string strRqDetail, out string strResult, out string strError)
        {
            int intErrorNumber = 0;
            string strRejectDesc = "";
            strResult = "";
            strError = "";

            ServiceHeader();//replace hasil header
            this._strXMLSend.Replace("[SERVICENAME]", "CIF_Inquiry_CIFSearchfromCreditCard_01697");
            this._strXMLSend.Replace("[OPERATIONCODE]", "000");
            this._strXMLSend.Replace("[REQUESTDETAILFIELD]", strRqDetail);
            this._strXMLSend.Replace("[MOREINDICATOR]", "N");

            bool blnResult;
            blnResult = false;
            try
            {
                wsProCIF.CIFInquiryCIFSearchfromCreditCard01697Response response = soapClient.CIFInquiryCIFSearchfromCreditCard01697Async(this._strGUID, this._strXMLSend.ToString(), this._strUserNIK).Result;
                strResult = response.Body.XMLResult;
                intErrorNumber = response.Body.ErrorNumber;
                strRejectDesc = response.Body.RejectDesc;
                strResult = strResult.Replace("ns1:", "");
                blnResult = true;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                blnResult = false;
            }

            if (strRejectDesc != "")
            {
                if (strRejectDesc.Trim().Contains("Data tidak ada di Database"))
                {
                    blnResult = true;
                }
                else
                {
                    blnResult = false;
                    strRejectDesc = "Error# " + intErrorNumber.ToString() + " " + strRejectDesc;
                }
                strError = strRejectDesc.Trim();
            }
            return blnResult;
        }

        #region Header List
        private void ServiceHeader()
        {
            //if (this._strGUID == null) MessageBox.Show("Warning Guid Kosong");
            if (String.IsNullOrEmpty(this._strGUID)) {if (String.IsNullOrEmpty(this._strGUID)) {this._strGUID = System.Guid.NewGuid().ToString();}}
            this._strXMLSend = new StringBuilder("<ns0:ServiceEnvelope xmlns:SOAP-ENV = \"http://schemas.xmlsoap.org/soap/envelope/\"");
            _strXMLSend.AppendLine(" xmlns:ns0 = \"http://schemas.ocbc.com/soa/emf/common/envelope/\">");
            _strXMLSend.AppendLine("<ns0:ServiceHeader>");
            _strXMLSend.AppendLine("<ns1:CommonDetail xmlns:ns1 = \"http://schemas.ocbc.com/soa/emf/common/header\">");
            _strXMLSend.AppendLine("<ns1:ServiceName>[SERVICENAME]</ns1:ServiceName>");
            _strXMLSend.AppendLine("<ns1:OperationCode>[OPERATIONCODE]</ns1:OperationCode>");
            _strXMLSend.AppendLine("<ns1:EMFVersion>000</ns1:EMFVersion>");
            _strXMLSend.AppendLine("<ns1:ServiceVersion>1.0</ns1:ServiceVersion>");
            _strXMLSend.AppendLine("<ns1:OperationVersion>000</ns1:OperationVersion>");
            _strXMLSend.AppendLine("<ns1:RqUuid>" + this._strGUID + "</ns1:RqUuid>");
            _strXMLSend.AppendLine("<ns1:RqDateTime>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff") + "</ns1:RqDateTime>");
            _strXMLSend.AppendLine("<ns1:RqExpiryTime>40</ns1:RqExpiryTime>");
            _strXMLSend.AppendLine("<ns1:RecordControl>");
            _strXMLSend.AppendLine("<ns1:ProviderControl>");
            _strXMLSend.AppendLine("<ns1:MoreIndicator>[MOREINDICATOR]</ns1:MoreIndicator>");
            _strXMLSend.AppendLine("</ns1:ProviderControl>");
            _strXMLSend.AppendLine("</ns1:RecordControl>");
            _strXMLSend.AppendLine("<ns1:RqNature>");
            _strXMLSend.AppendLine("<ns1:TransactionRefNo>0</ns1:TransactionRefNo>");
            _strXMLSend.AppendLine("<ns1:TransactionMode>000</ns1:TransactionMode>");
            _strXMLSend.AppendLine("<ns1:RqStatus>000</ns1:RqStatus>");
            _strXMLSend.AppendLine("</ns1:RqNature>");
            _strXMLSend.AppendLine("</ns1:CommonDetail>");
            _strXMLSend.AppendLine("<ns1:ClientDetail xmlns:ns1 = \"http://schemas.ocbc.com/soa/emf/common/header\">");
            _strXMLSend.AppendLine("<ns1:ID>NISP</ns1:ID>");
            _strXMLSend.AppendLine("<ns1:Organization>NISP</ns1:Organization>");
            _strXMLSend.AppendLine("<ns1:Subsystem>" + this._strSubSystem + "</ns1:Subsystem>");
            _strXMLSend.AppendLine("<ns1:RegionDetail>");
            _strXMLSend.AppendLine("<ns1:RegionCode>INA</ns1:RegionCode>");
            _strXMLSend.AppendLine("<ns1:CountryCode>ID</ns1:CountryCode>");
            _strXMLSend.AppendLine("</ns1:RegionDetail>");
            _strXMLSend.AppendLine("<ns1:UserDetail>");
            _strXMLSend.AppendLine("<ns1:UserId>" + this._strUserNIK + "</ns1:UserId>");//LIVE HRS PAKE INI
            //_strXMLSend.AppendLine("<ns1:UserId>" + "90000" + "</ns1:UserId>");// ke U2 cm bisa pake 90000
            _strXMLSend.AppendLine("<ns1:UserRole/>");
            _strXMLSend.AppendLine("<ns1:UserPassword/>");
            _strXMLSend.AppendLine("</ns1:UserDetail>");
            _strXMLSend.AppendLine("</ns1:ClientDetail>");
            _strXMLSend.AppendLine("</ns0:ServiceHeader>");
            _strXMLSend.AppendLine("<ns0:ServiceBody>");
            _strXMLSend.AppendLine("<ns1:RqDetail xmlns:ns1 = \"" + "http://schemas.nisp.com/soa/accounts/service/[SERVICENAME]" + "\">");
            _strXMLSend.AppendLine("[REQUESTDETAILFIELD]");
            _strXMLSend.AppendLine("</ns1:RqDetail>");
            _strXMLSend.AppendLine("</ns0:ServiceBody>");
            _strXMLSend.AppendLine("</ns0:ServiceEnvelope>");

        }
        #endregion

        #region DisposeObject
        // Flag: Has Dispose already been called?
        bool disposed = false;

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                //
            }
            disposed = true;
        }
        #endregion
    }
}
