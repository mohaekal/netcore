﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ProCIF.DataPribadi.API.Services
{

    public class clsDBServiceSQLServer : IDisposable
    {
        public bool ExecStoredProcedure(
            String ConnectionString,
            String StoredProcedureName,
            ref SqlParameter[] Param,
            int Timeout,
            out DataSet myDataSet)
        {
            bool bReturn = true;
            //SqlConnection _sqlConn = null;
            myDataSet = new DataSet();

            try
            {
                using (SqlConnection _sqlConn = new SqlConnection(ConnectionString))
                {
                    string _sql = StoredProcedureName;
                    using (SqlCommand _sqlCmnd = new SqlCommand(_sql, _sqlConn))
                    {
                        _sqlCmnd.CommandType = CommandType.StoredProcedure;
                        _sqlCmnd.Parameters.AddRange(Param);
                        _sqlCmnd.CommandTimeout = Timeout;
                        _sqlConn.Open();
                        using (SqlDataAdapter _sqlAdapter = new SqlDataAdapter(_sqlCmnd))
                        {
                            _sqlAdapter.Fill(myDataSet);
                        }
                        _sqlConn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bReturn;
        }

        public bool ExecStoredProcedure(
            String ConnectionString,
            String StoredProcedureName,
            int Timeout,
            out DataSet myDataSet)
        {
            bool bReturn = true;
            //SqlConnection _sqlConn = null;
            myDataSet = new DataSet();

            try
            {
                using (SqlConnection _sqlConn = new SqlConnection(ConnectionString))
                {
                    string _sql = StoredProcedureName;
                    using (SqlCommand _sqlCmnd = new SqlCommand(_sql, _sqlConn))
                    {
                        _sqlCmnd.CommandType = CommandType.StoredProcedure;
                        _sqlCmnd.CommandTimeout = Timeout;
                        _sqlConn.Open();
                        using (SqlDataAdapter _sqlAdapter = new SqlDataAdapter(_sqlCmnd))
                        {
                            _sqlAdapter.Fill(myDataSet);
                        }
                        _sqlConn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bReturn;
        }

        public bool QueryCommand(
            String ConnectionString,
            String SqlQuery,
            ref SqlParameter[] Param,
            int Timeout,
            out DataSet myDataSet,
            out string strError)
        {
            bool bReturn = true;
            //SqlConnection _sqlConn = null;
            myDataSet = new DataSet();
            strError = "";

            try
            {
                using (SqlConnection _sqlConn = new SqlConnection(ConnectionString))
                {
                    string _sql = SqlQuery;
                    using (SqlCommand _sqlCmnd = new SqlCommand(_sql, _sqlConn))
                    {
                        _sqlCmnd.CommandType = CommandType.Text;
                        _sqlCmnd.Parameters.AddRange(Param);
                        _sqlCmnd.CommandTimeout = Timeout;
                        _sqlConn.Open();
                        using (SqlDataAdapter _sqlAdapter = new SqlDataAdapter(_sqlCmnd))
                        {
                            _sqlAdapter.Fill(myDataSet);
                        }
                        _sqlConn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                bReturn = false;
            }

            return bReturn;
        }

        public bool QueryCommand(
            String ConnectionString,
            String SqlQuery,
            ref SqlParameter[] Param,
            int Timeout,
            out string strError)
        {
            bool bReturn = true;
            strError = "";

            try
            {
                using (SqlConnection _sqlConn = new SqlConnection(ConnectionString))
                {
                    string _sql = SqlQuery;
                    using (SqlCommand _sqlCmnd = new SqlCommand(_sql, _sqlConn))
                    {
                        _sqlCmnd.CommandType = CommandType.Text;
                        _sqlCmnd.Parameters.AddRange(Param);
                        _sqlCmnd.CommandTimeout = Timeout;
                        _sqlConn.Open();
                        _sqlCmnd.ExecuteNonQuery();
                        _sqlConn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                bReturn = false;
            }

            return bReturn;
        }

        public bool QueryCommand(
            String ConnectionString,
            String SqlQuery,
            int Timeout,
            out string strError)
        {
            bool bReturn = true;
            strError = "";

            try
            {
                using (SqlConnection _sqlConn = new SqlConnection(ConnectionString))
                {
                    string _sql = SqlQuery;
                    using (SqlCommand _sqlCmnd = new SqlCommand(_sql, _sqlConn))
                    {
                        _sqlCmnd.CommandType = CommandType.Text;
                        _sqlCmnd.CommandTimeout = Timeout;
                        _sqlConn.Open();
                        _sqlCmnd.ExecuteNonQuery();
                        _sqlConn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                bReturn = false;
            }

            return bReturn;
        }

        public bool QueryCommand(
            String ConnectionString,
            String SqlQuery,
            int Timeout,
            out DataSet myDataSet,
            out string strError)
        {
            bool bReturn = true;
            //SqlConnection _sqlConn = null;
            myDataSet = new DataSet();
            strError = "";

            try
            {
                using (SqlConnection _sqlConn = new SqlConnection(ConnectionString))
                {
                    string _sql = SqlQuery;
                    using (SqlCommand _sqlCmnd = new SqlCommand(_sql, _sqlConn))
                    {
                        _sqlCmnd.CommandType = CommandType.Text;
                        _sqlCmnd.CommandTimeout = Timeout;
                        _sqlConn.Open();
                        using (SqlDataAdapter _sqlAdapter = new SqlDataAdapter(_sqlCmnd))
                        {
                            _sqlAdapter.Fill(myDataSet);
                        }
                        _sqlConn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                bReturn = false;
            }

            return bReturn;
        }

        #region DisposeObject
        // Flag: Has Dispose already been called?
        bool disposed = false;

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                //
            }
            disposed = true;
        }
        #endregion
    }
}
