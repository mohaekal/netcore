﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ProCIF.DataPribadi.API.Services
{

    public static class DateTimeLib
    {
        private static CultureInfo _cultureInfo = new CultureInfo("en-US");
        private static DateTimeStyles _dateTimeStyle = DateTimeStyles.AdjustToUniversal;

        public static DateTimeStyles DateTimeStyle
        {
            get { return DateTimeLib._dateTimeStyle; }
            set { DateTimeLib._dateTimeStyle = value; }
        }
        public static CultureInfo CultureInfo
        {
            get { return DateTimeLib._cultureInfo; }
            set { DateTimeLib._cultureInfo = value; }
        }

        public static DateTime GetDateTime(int dateInt, string dateFormat)
        {
            return GetDateTime(dateInt.ToString(), dateFormat);
        }

        public static DateTime GetDateTime(string dateString, string dateFormat)
        {
            DateTime dateTimeResult;
            if (DateTime.TryParseExact(dateString, dateFormat, _cultureInfo, _dateTimeStyle, out dateTimeResult))
            {
                return dateTimeResult;
            }
            else if (DateTime.TryParseExact(dateString.PadLeft(dateFormat.Length, '0'), dateFormat, _cultureInfo, _dateTimeStyle, out dateTimeResult))
            {
                return dateTimeResult;
            }
            else
            {
                int i = 1;
                while (dateFormat[i] == dateFormat[0]) i++;
                dateFormat = dateFormat.Substring(i - 1);
                if (DateTime.TryParseExact(dateString, dateFormat, _cultureInfo, _dateTimeStyle, out dateTimeResult))
                {
                    return dateTimeResult;
                }
            }
            return new DateTime();

        }

        public static int GetDateTimeInt(int dateInt, string dateFormat, string dateResultFormat)
        {
            DateTime dateTime = GetDateTime(dateInt.ToString(), dateFormat);
            if (dateTime != new DateTime())
            {
                return int.Parse(dateTime.ToString(dateResultFormat));
            }
            else
            {
                return 0;
            }
        }

        public static int GetDateTimeInt(string dateString, string dateFormat, string dateResultFormat)
        {
            DateTime dateTime = GetDateTime(dateString, dateFormat);
            if (dateTime != new DateTime())
            {
                return int.Parse(dateTime.ToString(dateResultFormat));
            }
            else
            {
                return 0;
            }
        }

        public static string GetDateTimeString(int dateInt, string dateFormat, string dateResultFormat)
        {
            DateTime dateTime = GetDateTime(dateInt.ToString(), dateFormat);
            if (dateTime != new DateTime())
            {
                return dateTime.ToString(dateResultFormat);
            }
            else
            {
                return "";
            }
        }

        public static string GetDateTimeString(string dateString, string dateFormat, string dateResultFormat)
        {
            DateTime dateTime = GetDateTime(dateString, dateFormat);
            if (dateTime != new DateTime())
            {
                return dateTime.ToString(dateResultFormat);
            }
            else
            {
                return "";
            }
        }

        public static int GetJulian(DateTime date)
        {
            int years = date.Year * 1000;
            int days = date.DayOfYear;
            return years + days;
        }

        public static int GetJulian(int dateInt, string dateFormat)
        {
            DateTime date = GetDateTime(dateInt, dateFormat);
            int years = date.Year * 1000;
            int days = date.DayOfYear;
            return years + days;
        }

        public static int GetJulian(string dateString, string dateFormat)
        {
            DateTime date = GetDateTime(dateString, dateFormat);
            int years = date.Year * 1000;
            int days = date.DayOfYear;
            return years + days;
        }

        public static DateTime GetDatetimeFromJulian(string julianFormat)
        {
            string hariKe;
            string tahun;
                        
            hariKe = julianFormat.Substring(julianFormat.Length - 3, 3);
            tahun = julianFormat.Substring(0, 4);

            DateTime tmp = new DateTime(int.Parse(tahun), 1, 1);

            tmp = tmp.AddDays(int.Parse(hariKe) - 1);

            return tmp;
        }
    }
}

