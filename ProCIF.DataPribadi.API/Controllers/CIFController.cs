﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using APILogging.NetCoreLibrary.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using local = ProCIF.DataPribadi.API.Models;
using ProCIF.DataPribadi.API.Services;
using ProCIF.Model;

namespace ProCIF.DataPribadi.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CIFController : ControllerBase
    {       
        private IService APIService;

        public CIFController(IService service)
        {
            APIService = service;
        }

        [HttpGet]
        public local.APIMessage<string> Get()
        {
            local.APIMessage<string> message = new local.APIMessage<string>
            {
                IsSuccess = true,
                Data = "You're In Data Pribadi API",
                Description = ""
            };
            return message;
        }
        
        [HttpPost("[action]")]
        public ActionResult<local.APIMessage<GeneralCIFModel>> GeneralCreateCIF([FromBody]GeneralCIFModel inModel)
        {
            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Starting : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, inModel, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();

            using (clsCustomer _clsCustomer = new clsCustomer(APIService))
            {
                msgResponse = _clsCustomer.GeneralCreateCIF(inModel);
            }

            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Finished : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, msgResponse, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            return msgResponse;
        }

        [HttpPost("[action]")]
        public ActionResult<local.APIMessage<GeneralCIFModel>> QuickCreation([FromBody]GeneralCIFModel inModel)
        {
            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Starting : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, inModel, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();

            using (clsCustomer _clsCustomer = new clsCustomer(APIService))
            {
                msgResponse = _clsCustomer.QuickCreation(inModel);
            }

            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Finished : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, msgResponse, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            return msgResponse;
        }

        [HttpPost("[action]")]
        public ActionResult<local.APIMessageDetail<GeneralCIFModel>> MaintenanceWithApproval([FromBody]GeneralCIFModel inModel)
        {
            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Starting : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, inModel, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            local.APIMessageDetail<GeneralCIFModel> msgResponse = new local.APIMessageDetail<GeneralCIFModel>();

            using (clsCustomer _clsCustomer = new clsCustomer(APIService))
            {
                msgResponse = _clsCustomer.MaintenanceWithApproval(inModel);
            }

            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Finished : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, msgResponse, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);

            return msgResponse;
        }
        
        [HttpPost("[action]")]
        public ActionResult<local.APIMessage<string>> UpdateDataFromApproval([FromBody]KAFKAModel inModel)
        {
            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Starting : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, inModel, null);
            local.APIMessage<string> msgResponse = new local.APIMessage<string>();

            using (clsCustomer _clsCustomer = new clsCustomer(APIService))
            {
                msgResponse = _clsCustomer.UpdateDataFromApproval(inModel);
            }

            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Finished : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, msgResponse, null);
            return msgResponse;
        }
                
        [HttpPost("[action]")]
        public ActionResult<local.APIMessage<List<ScreeningListModel>>> ScreeningAML([FromBody]ScreeningListModel inModel)
        {
            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Starting : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, inModel, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            local.APIMessage<List<ScreeningListModel>> msgResponse = new local.APIMessage<List<ScreeningListModel>>();

            using (clsAMLScreeningList _clsAMLScreeningList = new clsAMLScreeningList(APIService))
            {
                msgResponse = _clsAMLScreeningList.CheckScreeningList(inModel);
            }

            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Finished : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, msgResponse, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            return msgResponse;            
        }
                
        [HttpGet("[action]/{paramKodeNegara}")]
        public ActionResult<local.APIMessage<DataTable>> ScreeningNegara(string paramKodeNegara)
        {
            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Starting : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, paramKodeNegara, null);
            local.APIMessage<DataTable> msgResponse = new local.APIMessage<DataTable>();

            using (clsAMLScreeningList _clsAMLScreeningList = new clsAMLScreeningList(APIService))
            {
                msgResponse = _clsAMLScreeningList.SubCheckScreeningNegara(paramKodeNegara);
            }

            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Finished : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, msgResponse, null);
            return msgResponse;
        }

        [HttpPost("[action]")]
        public ActionResult<local.APIMessage<GeneralCIFModel>> BasicMaintenance([FromBody]GeneralCIFModel inModel)
        {
            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Starting : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, inModel, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();

            using (clsCustomer _clsCustomer = new clsCustomer(APIService))
            {
                msgResponse = _clsCustomer.BasicMaintenance(inModel, true); //jika update langsung menggunakan api ini maka dianggap auto approve
            }

            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Finished : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, msgResponse, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            return msgResponse;
        }

        [HttpPost("[action]")]
        public ActionResult<local.APIMessage<GeneralCIFModel>> InquiryDetail([FromBody] GeneralCIFModel inModel)
        {
            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Starting : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, inModel, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();

            using (clsCustomer _clsCustomer = new clsCustomer(APIService))
            {
                msgResponse = _clsCustomer.InquiryDetail(inModel);
            }

            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Finished : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, msgResponse, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            return msgResponse;
        }

        [HttpPost("[action]")]
        public ActionResult<local.APIMessage<GeneralCIFModel>> InquiryDetailV2([FromBody] GeneralCIFModel inModel)
        {
            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Starting : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, inModel, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();

            using (clsCustomer _clsCustomer = new clsCustomer(APIService))
            {
                msgResponse = _clsCustomer.InquiryDetailV2(inModel);
            }

            APIService.doLogging(API_LOGENVELOPModel.INFO, "Request Finished : " + APIService.GetMethodName(), this.HttpContext.Request.Path.Value, msgResponse, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);
            return msgResponse;
        }

    }
}