﻿using local = ProCIF.DataPribadi.API.Models;
using ProCIF.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Newtonsoft.Json;
using ProCIF.DataPribadi.API.Services;
using DateTimeLib = ProCIF.DataPribadi.API.Services.DateTimeLib;
using APILogging.NetCoreLibrary.Models;

namespace ProCIF.DataPribadi.API.Controllers
{
    public class clsCustomer : IDisposable
    {
        IService _service;
        public clsCustomer(IService service)
        {
            _service = service;
        }
       
        public local.APIMessage<GeneralCIFModel> GeneralCreateCIF(GeneralCIFModel createCIFModel)
        {
            //**********************************************************************************
            //dimethod ini tidak ada KYC dan checking AML wajib di orchestrator oleh frontend
            //**********************************************************************************
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

            local.APIMessage<GeneralCIFModel> msgResponseFinal = new local.APIMessage<GeneralCIFModel>();
            local.APIMessage<GeneralCIFModel> msgResponseQuickCreation = new local.APIMessage<GeneralCIFModel>();
            local.APIMessage<GeneralCIFModel> msgResponseMaintenance = new local.APIMessage<GeneralCIFModel>();

            msgResponseFinal.IsSuccess = true;
            msgResponseFinal.Description = "";

            local.APIMessage<string> msgResponseValidasi1 = new local.APIMessage<string>();
            local.APIMessage<string> msgResponseValidasi2 = new local.APIMessage<string>();

            SubSetDefaultValueModel(createCIFModel);

            msgResponseValidasi1 = this.ValidateQuickCreation(createCIFModel);

            msgResponseValidasi2 = this.ValidateMaintenanceCIF(createCIFModel);

            if (!string.IsNullOrEmpty(msgResponseValidasi1.Description))
            {
                msgResponseValidasi1.IsSuccess = false;
            }

            if (!string.IsNullOrEmpty(msgResponseValidasi2.Description))
            {
                msgResponseValidasi2.IsSuccess = false;
            }

            if (!msgResponseValidasi1.IsSuccess)
            {
                msgResponseFinal.Description = msgResponseValidasi1.Description;
                msgResponseFinal.IsSuccess = false;
            }

            if (!msgResponseValidasi2.IsSuccess)
            {
                if (string.IsNullOrEmpty(msgResponseFinal.Description))
                {
                    msgResponseFinal.Description = msgResponseValidasi2.Description;
                }
                else
                {
                    msgResponseFinal.Description = msgResponseFinal.Description + Environment.NewLine + msgResponseValidasi2.Description;
                }
                msgResponseFinal.IsSuccess = false;
            }

            if (!msgResponseFinal.IsSuccess)
                return msgResponseFinal;

            msgResponseQuickCreation = _service.QuickCreation(createCIFModel);
            
            if (msgResponseQuickCreation.IsSuccess)
            {
                createCIFModel.CustomerNumber = msgResponseQuickCreation.Data.CustomerNumber;
                //_service.doLogging(API_LOGENVELOPModel.INFO, "Created CIF SUCCESS IN : " + _service.GetMethodName(), "", createCIFModel.CustomerNumber, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

                msgResponseMaintenance = _service.BasicMaintenance(createCIFModel, true);

                if (msgResponseMaintenance.IsSuccess)
                {
                    msgResponseFinal.Data = msgResponseMaintenance.Data;
                }
                else
                {
                    msgResponseFinal.Description = "Proses Quick Creation berhasil dan Proses Maintenance Gagal silahkan lakukan update manual dicabang pada menu aplikasi ProCIF - Maintenance Data Pribadi Nasabah, Nomor CIF : " + msgResponseQuickCreation.Data.CustomerNumber;
                }

                msgResponseFinal.IsSuccess = msgResponseMaintenance.IsSuccess;                                

                return msgResponseFinal;
            }
            else
            {
                msgResponseFinal.IsSuccess = msgResponseQuickCreation.IsSuccess;
                msgResponseFinal.Description = msgResponseQuickCreation.Description;
                return msgResponseFinal;
            }
        }

        public local.APIMessage<GeneralCIFModel> QuickCreation(GeneralCIFModel createCIFModel)
        {
            //**********************************************************************************
            //dimethod ini tidak ada KYC dan checking AML wajib di orchestrator oleh frontend
            //**********************************************************************************
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

            local.APIMessage<GeneralCIFModel> msgResponseFinal = new local.APIMessage<GeneralCIFModel>();
            local.APIMessage<GeneralCIFModel> msgResponseQuickCreation = new local.APIMessage<GeneralCIFModel>();

            msgResponseFinal.IsSuccess = true;
            msgResponseFinal.Description = "";

            local.APIMessage<string> msgResponseValidasi1 = new local.APIMessage<string>();

            SubSetDefaultValueModel(createCIFModel);

            msgResponseValidasi1 = this.ValidateQuickCreation(createCIFModel);

            if (!string.IsNullOrEmpty(msgResponseValidasi1.Description))
            {
                msgResponseValidasi1.IsSuccess = false;
            }
            
            if (!msgResponseValidasi1.IsSuccess)
            {
                msgResponseFinal.Description = msgResponseValidasi1.Description;
                msgResponseFinal.IsSuccess = false;
            }
            
            if (!msgResponseFinal.IsSuccess)
                return msgResponseFinal;

            msgResponseFinal = _service.QuickCreation(createCIFModel);

            return msgResponseFinal;
        }

        public local.APIMessage<GeneralCIFModel> BasicMaintenance(GeneralCIFModel createCIFModel, bool isAutoApprove = false)
        {
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);
                       
            local.APIMessage<GeneralCIFModel> msgResponseFinal = new local.APIMessage<GeneralCIFModel>();
            local.APIMessage<GeneralCIFModel> msgResponseBasicMaintenance = new local.APIMessage<GeneralCIFModel>();

            msgResponseFinal.IsSuccess = true;
            msgResponseFinal.Description = "";

            local.APIMessage<string> msgResponseValidasi1 = new local.APIMessage<string>();

            SubSetDefaultValueModel(createCIFModel);

            msgResponseValidasi1 = this.ValidateMaintenanceCIF(createCIFModel);

            if (!string.IsNullOrEmpty(msgResponseValidasi1.Description))
            {
                msgResponseValidasi1.IsSuccess = false;
            }

            if (!msgResponseValidasi1.IsSuccess)
            {
                msgResponseFinal.Description = msgResponseValidasi1.Description;
                msgResponseFinal.IsSuccess = false;
            }

            if (!msgResponseFinal.IsSuccess)
                return msgResponseFinal;

            msgResponseFinal = _service.BasicMaintenance(createCIFModel, isAutoApprove);

            return msgResponseFinal;
        }        
        
        public local.APIMessage<string> UpdateDataFromApproval(KAFKAModel requestModel)
        {
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, requestModel.TrxGUID);

            local.APIMessage<GeneralCIFModel> msgUpdateCore = new local.APIMessage<GeneralCIFModel>();
            local.APIMessage<ApprovalMasterModel> msgApprovalGet = new local.APIMessage<ApprovalMasterModel>();
            local.APIMessage<ApprovalMasterModel> msgApprovalUpdate = new local.APIMessage<ApprovalMasterModel>();
            local.APIMessage<string> msgFinalResponse = new local.APIMessage<string>();
            GeneralCIFModel createCIFModel = new GeneralCIFModel();
            try
            {
                msgApprovalGet = _service.SubGetDataChangeMasterTM(requestModel.JSONData.masterModels[0].ChangeId);

                if (msgApprovalGet.IsSuccess && msgApprovalGet.Data.Status == "PENDING")
                {
                    createCIFModel = JsonConvert.DeserializeObject<GeneralCIFModel>(msgApprovalGet.Data.JsonDataNew);

                    msgApprovalGet.Data.ApprovedDate = requestModel.DateAuthorized;
                    msgApprovalGet.Data.SpvNIK = requestModel.SPV;
                    if (requestModel.AuthStatus == "2")
                    {
                        msgApprovalGet.Data.Status = "APPROVE";
                    }
                    else
                    {
                        msgApprovalGet.Data.Status = "REJECT";
                    }
                    msgApprovalGet.Data.Description = requestModel.SPVMessage;

                    //IF KAFKA RESPONSE APPROVE
                    if (requestModel.AuthStatus == "2")
                    {
                        msgApprovalGet.Data.HostDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        msgUpdateCore = _service.BasicMaintenance(createCIFModel);
                        msgApprovalGet.Data.HostDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        msgApprovalGet.Data.Description = msgUpdateCore.Description;

                        if (msgUpdateCore.IsSuccess)
                        {
                            msgFinalResponse.IsSuccess = true;
                            msgFinalResponse.Description = "";
                            msgFinalResponse.Data = "SUCCESS";

                            msgApprovalUpdate = _service.SubUpdateDataChangeMasterTM(msgApprovalGet.Data);

                            //PUT KE KAFKA TOPIC PROCESS_APPROVAL
                            requestModel.TrxStatus = "2";
                            requestModel.TrxStatusDetail = "";
                        }
                        else
                        {
                            msgFinalResponse.IsSuccess = false;
                            msgFinalResponse.Description = msgUpdateCore.Description;
                            msgFinalResponse.Data = "FAILED";

                            //PUT KE KAFKA TOPIC PROCESS_APPROVAL
                            requestModel.TrxStatus = "3";
                            requestModel.TrxStatusDetail = msgUpdateCore.Description;
                        }
                    }
                    else //REJECT
                    {
                        msgApprovalGet.Data.HostDate = "-";
                        msgApprovalGet.Data.Description = "";

                        msgFinalResponse.IsSuccess = true;
                        msgFinalResponse.Description = "";
                        msgFinalResponse.Data = "SUCCESS";

                        msgApprovalUpdate = _service.SubUpdateDataChangeMasterTM(msgApprovalGet.Data);

                        //PUT KE KAFKA TOPIC PROCESS_APPROVAL
                        requestModel.TrxStatus = "2";
                        requestModel.TrxStatusDetail = "";
                    }

                    if (msgApprovalUpdate.IsSuccess)
                    {
                        msgFinalResponse.IsSuccess = true;
                        msgFinalResponse.Description = "";
                        msgFinalResponse.Data = "SUCCESS";
                    }
                    else
                    {
                        msgFinalResponse.IsSuccess = true;
                        msgFinalResponse.Description = "FAILED ON SubUpdateDataChangeMasterTM : " + msgApprovalUpdate.Description;
                        msgFinalResponse.Data = "SUCCESS";
                    }

                    //PUT KE KAFKA TOPIC PROCESS_APPROVAL
                    string jsonKAFKA = JsonConvert.SerializeObject(requestModel);
                    local.APIMessage<bool> msgResult = new local.APIMessage<bool>();
                    msgResult = _service.PUTMessageToProcessKAFKA(jsonKAFKA);

                    if (msgResult.IsSuccess)
                    {
                        msgFinalResponse.IsSuccess = true;
                        msgFinalResponse.Data = "SUCCESS";
                        msgFinalResponse.Description = "";
                    }
                    else
                    {
                        msgFinalResponse.IsSuccess = true;
                        msgFinalResponse.Data = "SUCCESS";
                        msgFinalResponse.Description = "FAILED ON PUTMessageToDoneKAFKA";
                    }
                }
                else
                {
                    msgFinalResponse.IsSuccess = false;
                    msgFinalResponse.Description = "Data ini sudah diapprove/direject sebelumnya!";
                    msgFinalResponse.Data = "FAILED";
                }
            }
            catch (Exception ex)
            {
                msgFinalResponse.IsSuccess = false;
                msgFinalResponse.Data = "FAILED";
                msgFinalResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                _service.doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + _service.GetMethodName(),"","",ex, requestModel.TrxGUID);
            }
            return msgFinalResponse;
        }

        public local.APIMessageDetail<GeneralCIFModel> MaintenanceWithApproval(GeneralCIFModel createCIFModel)
        {
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

            local.APIMessageDetail<GeneralCIFModel> msgResponse = new local.APIMessageDetail<GeneralCIFModel>();

            try
            {
                SubSetDefaultValueModel(createCIFModel);

                local.APIMessage<string> msgValidate = new local.APIMessage<string>();
                msgValidate = this.ValidateMaintenanceCIF(createCIFModel);

                //validasi dulu logic bisnis nya
                if (!msgValidate.IsSuccess)
                {
                    msgResponse.IsSuccess = msgValidate.IsSuccess;
                    msgResponse.Data = null;
                    msgResponse.Description = msgValidate.Description;
                    return msgResponse;
                }

                #region "CompareValue"

                local.APIMessage<DataSet> msgDB = new local.APIMessage<DataSet>();
                msgDB = _service.SubCheckApproval(createCIFModel.CustomerNumber, "CFMAST", createCIFModel.GUID, "U");

                if (msgDB.IsSuccess && msgDB.Data != null && msgDB.Data.Tables.Count > 0)
                {
                    if (msgDB.Data.Tables[0].Rows.Count > 0)
                    {
                        msgResponse.Description = "Data CIF Maintenance sebelumnya belum dilakukan Otorisasi oleh SPV!";
                        msgResponse.IsSuccess = false;
                        msgResponse.IsNeedApproval = true;
                        msgResponse.Data = null;
                        return msgResponse;
                    }
                    else
                    {
                        GeneralCIFModel modelOld = new GeneralCIFModel();
                        modelOld = _service.InquiryDetailV2(createCIFModel).Data;

                        if (modelOld == null)
                        {
                            throw new Exception("Inquiry Detail Model Data Old not found!");
                        }

                        local.APIMessage<ApprovalMasterModel> masterModel = new local.APIMessage<ApprovalMasterModel>();
                        List<ApprovalDetailModel> listApprovalDetailModel = new List<ApprovalDetailModel>();

                        masterModel = _service.ProcessCompareOldAndNewModel(createCIFModel, modelOld, ref listApprovalDetailModel);

                        #endregion

                        if (masterModel.IsSuccess && masterModel.Data != null && listApprovalDetailModel != null && listApprovalDetailModel.Count > 0)                        
                        {
                            //dioverride lagi disini
                            masterModel.Data.Branch = createCIFModel.Branch;
                            masterModel.Data.ChangeType = "U";
                            masterModel.Data.CIFName = createCIFModel.CustomerName1;
                            masterModel.Data.CIFNumber = createCIFModel.CustomerNumber;
                            masterModel.Data.GUID = createCIFModel.GUID;
                            string jsonNew = JsonConvert.SerializeObject(createCIFModel);
                            string jsonOld = JsonConvert.SerializeObject(modelOld);
                            masterModel.Data.JsonDataNew = jsonNew;
                            masterModel.Data.JsonDataOld = jsonOld;
                            masterModel.Data.StaffNIK = createCIFModel.NIK;
                            masterModel.Data.Status = "PENDING";
                            masterModel.Data.SubSystem = createCIFModel.Module;
                            _service.doLogging(API_LOGENVELOPModel.INFO, "Data Checking masterModel : " + _service.GetMethodName(), "", masterModel, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);
                            _service.doLogging(API_LOGENVELOPModel.INFO, "Data Checking listApprovalDetailModel : " + _service.GetMethodName(), "", listApprovalDetailModel, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);


                            msgResponse = _service.SubSaveApprovalMaintenanceCIF(masterModel.Data, listApprovalDetailModel);

                            if (msgResponse.IsSuccess)
                            {
                                msgResponse.IsSuccess = true;
                                msgResponse.IsNeedApproval = true;
                                msgResponse.Description = "Data CIF Maintenance butuh Approval Supervisor!";
                            }
                        }
                        else
                        {
                            msgResponse.IsSuccess = false;
                            msgResponse.IsNeedApproval = false;
                            msgResponse.Description = "Tidak ada perubahan data cif maintenance!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msgResponse.IsSuccess = false;
                msgResponse.IsNeedApproval = false;
                //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                _service.doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + _service.GetMethodName(), "", null, ex, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

            }

            return msgResponse;
        }

        public local.APIMessage<GeneralCIFModel> InquiryDetail(GeneralCIFModel inModel)
        {
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);

            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();
            GeneralCIFModel paramModel = new GeneralCIFModel();
            paramModel.CustomerNumber = inModel.CustomerNumber;
            paramModel.GUID = inModel.GUID;
            paramModel.NIK = inModel.NIK;
            paramModel.Module = inModel.Module;
            paramModel.MoreIndicator = inModel.MoreIndicator;
            paramModel.Branch = inModel.Branch;

            msgResponse = _service.InquiryDetail(paramModel);

            return msgResponse;
        }

        public local.APIMessage<GeneralCIFModel> InquiryDetailV2(GeneralCIFModel inModel)
        {
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, inModel.GUID, inModel.NIK, inModel.Module, inModel.Branch);

            local.APIMessage<GeneralCIFModel> msgResponse = new local.APIMessage<GeneralCIFModel>();
            GeneralCIFModel paramModel = new GeneralCIFModel();
            paramModel.CustomerNumber = inModel.CustomerNumber.ToString();
            paramModel.GUID = inModel.GUID;
            paramModel.NIK = inModel.NIK;
            paramModel.Module = inModel.Module;
            paramModel.MoreIndicator = inModel.MoreIndicator;
            paramModel.Branch = inModel.Branch;

            msgResponse = _service.InquiryDetailV2(paramModel);

            return msgResponse;
        }
        
        private void SubSetDefaultValueModel(GeneralCIFModel createCIFModel)
        {
            //buat set default disini :
            if (createCIFModel.CustomerTypeCode.Equals("C"))
            {
                createCIFModel.TaxCode = "0";
            }
            else
            {
                createCIFModel.TaxCode = "5";
            }

            if (createCIFModel.ForeignAddress == "N")
            {
                createCIFModel.ForeignAddress = "N";
                createCIFModel.Addressline4 = createCIFModel.Kelurahan + ", " + createCIFModel.DatiII + ' ' + createCIFModel.Postalcode;
            }

            if (createCIFModel.CountryOfResidence.Equals("000"))
            {
                createCIFModel.ResidentCode = "Y";
            }
            else
            {
                createCIFModel.ResidentCode = "N";
            }
            createCIFModel.HighriskUser = createCIFModel.NIK;

            local.APIMessage<CIFCurrentDate> dsCurrenctDate = new local.APIMessage<CIFCurrentDate>();
            dsCurrenctDate = this.GetCurrentDate();
            if (dsCurrenctDate.IsSuccess)
            {
                createCIFModel.TransactionDate = dsCurrenctDate.Data.TransactionDate;
                createCIFModel.OriginalCustomerDate = dsCurrenctDate.Data.TransactionDate;
                createCIFModel.ReviewDateDMY = dsCurrenctDate.Data.TransactionDate.ToString("ddMMyy");
                createCIFModel.DateOfLastMaintenance = dsCurrenctDate.Data.TransactionDate.ToString("ddMMyy");
                createCIFModel.LastChangeDate = dsCurrenctDate.Data.TransactionDate;
                createCIFModel.InformationDate = dsCurrenctDate.Data.TransactionDate;
            }
            else
            {
                createCIFModel.TransactionDate = DateTime.Now;
                createCIFModel.OriginalCustomerDate = DateTime.Now;
                createCIFModel.LastChangeDate = DateTime.Now;
                createCIFModel.ReviewDateDMY = DateTime.Now.ToString("ddMMyy");
                createCIFModel.DateOfLastMaintenance = DateTime.Now.ToString("ddMMyy");
                createCIFModel.InformationDate = DateTime.Now;
            }

            createCIFModel.LastChangedByuser = createCIFModel.NIK;

            if (createCIFModel.IDTypeCode == "KTP")
            {
                if (createCIFModel.IDNumber.Length == 16)
                {
                    int nojeniskel = Convert.ToInt32(createCIFModel.IDNumber.Substring(6, 2));
                    if (nojeniskel >= 1 && nojeniskel <= 31)
                    {
                        createCIFModel.Sexcode = "M";

                    }
                    else if (nojeniskel >= 41 && nojeniskel <= 71)
                    {
                        createCIFModel.Sexcode = "F";
                    }
                }
            }
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

        }

        public local.APIMessage<string> ValidateQuickCreation(GeneralCIFModel createCIFModel)
        {
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);


            bool isValidate = true;
            string strError = "";
            string tmp = "";
            local.APIMessage<string> msgValidate = new local.APIMessage<string>();

            //NIK RM tidak mandatory di API ini
            if (String.IsNullOrEmpty(createCIFModel.BranchNumber.Trim()))
            {
                strError += Environment.NewLine + "Branch Number cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.CustomerTypeCode.Trim()))
            {
                strError += Environment.NewLine + "Customer Type Code cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.CustomerName1.Trim()))
            {
                strError += Environment.NewLine + "Customer Name 1 cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.BirthPlace.Trim()))
            {
                strError += Environment.NewLine + "Birth Place cannot be empty!";
                isValidate = false;
            }

            if (createCIFModel.BirthDate.Year <= 1900)
            {
                strError += Environment.NewLine + "Birth Date cannot be empty!";
                isValidate = false;
            }
            else
            {
                if (this.SubValidateGreaterDate(createCIFModel.BirthDate))
                {
                    strError += Environment.NewLine + "Birth Date cannot be greater than today's date!";
                    isValidate = false;
                }
            }

            if (String.IsNullOrEmpty(createCIFModel.IDTypeCode.Trim()))
            {
                strError += Environment.NewLine + "Type Of Identity cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.IDNumber.Trim()))
            {
                strError += Environment.NewLine + "Number Of Identity cannot be empty!";
                isValidate = false;
            }

            if (createCIFModel.CustomerTypeCode.Trim() == "A")
            {
                if (String.IsNullOrEmpty(createCIFModel.MotherMaidenName.Trim()))
                {
                    strError += Environment.NewLine + "Mother Mainden Name cannot be empty!";
                    isValidate = false;
                }

                if (!this.SubValidasiKarakterASCIIExtended(createCIFModel.MotherMaidenName, "Mother Mainden Name", out tmp))
                {
                    strError += Environment.NewLine + tmp;
                    isValidate = false;
                }
            }

            if (String.IsNullOrEmpty(createCIFModel.Addressline1.Trim()))
            {
                strError += Environment.NewLine + "Address Line 1 cannot be empty!";
                isValidate = false;
            }

            if (createCIFModel.ForeignAddress == "N")
            {
                if (String.IsNullOrEmpty(createCIFModel.Postalcode.Trim()))
                {
                    strError += Environment.NewLine + "Postal code cannot be empty!";
                    isValidate = false;
                }

                if (String.IsNullOrEmpty(createCIFModel.Kelurahan.Trim()))
                {
                    strError += Environment.NewLine + "Kelurahan code cannot be empty!";
                    isValidate = false;
                }

                if (String.IsNullOrEmpty(createCIFModel.Kecamatan.Trim()))
                {
                    strError += Environment.NewLine + "Kecamatan code cannot be empty!";
                    isValidate = false;
                }

                if (String.IsNullOrEmpty(createCIFModel.DatiII.Trim()))
                {
                    strError += Environment.NewLine + "Dati II code cannot be empty!";
                    isValidate = false;
                }

                if (String.IsNullOrEmpty(createCIFModel.Provinsi.Trim()))
                {
                    strError += Environment.NewLine + "Provinsi code cannot be empty!";
                    isValidate = false;
                }
            }

            if (createCIFModel.Postalcode == "88888")
            {
                strError += Environment.NewLine + "Postal code cannot be use 88888!";
                isValidate = false;
            }

            if (!this.SubValidasiKarakterASCIIExtended(createCIFModel.Addressline1, "Address 1", out tmp))
            {
                strError += Environment.NewLine + tmp;
                isValidate = false;
            }

            if (!this.SubValidasiKarakterASCIIExtended(createCIFModel.Addressline2, "Address 2", out tmp))
            {
                strError += Environment.NewLine + tmp;
                isValidate = false;
            }

            if (!this.SubValidasiKarakterASCIIExtended(createCIFModel.Addressline3, "Address 3", out tmp))
            {
                strError += Environment.NewLine + tmp;
                isValidate = false;
            }

            local.APIMessage<DataSet> dsOutValidate = new local.APIMessage<DataSet>();
            dsOutValidate = _service.SubCheckIDTypeWithCustomerType(createCIFModel.CustomerTypeCode, createCIFModel.IDTypeCode);
            if (dsOutValidate.IsSuccess)
            {
                if (dsOutValidate.Data != null && dsOutValidate.Data.Tables.Count > 0 && dsOutValidate.Data.Tables[0].Rows.Count == 0)
                {
                    strError += Environment.NewLine + "Combinations Customer Type Code and Type Of Identity are not found!";
                    isValidate = false;
                }
            }
            else
            {
                strError += Environment.NewLine + dsOutValidate.Description;
                isValidate = false;
            }

            if (this.SubValidateIsNotNumber(createCIFModel.TitleBeforeName))
            {
                strError += Environment.NewLine + "Title Before Name cannot contain numbers";
                isValidate = false;
            }

            if (this.SubValidateIsNotNumber(createCIFModel.TitleAfterName))
            {
                strError += Environment.NewLine + "Title After Name cannot contain numbers";
                isValidate = false;
            }

            if (this.SubValidateFirstIsNotSpace(createCIFModel.CustomerName1))
            {
                strError += Environment.NewLine + "Customer Name 1 cannot contain space in first character!";
                isValidate = false;
            }

            if (!String.IsNullOrEmpty(createCIFModel.CustomerName1) && this.SubValidateIsNotSymbol(createCIFModel.CustomerName1.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Customer Name 1 cannot contain symbol in first character!";
                isValidate = false;
            }

            if (this.SubValidateCannotContainsTitle(createCIFModel.CustomerName1 + createCIFModel.CustomerName2))
            {
                strError += Environment.NewLine + "Customer Name cannot contain title before or title after!";
                isValidate = false;
            }

            if (this.SubValidateFirstIsNotSpace(createCIFModel.MotherMaidenName))
            {
                strError += Environment.NewLine + "Mother Maiden Name cannot contain space in first character!";
                isValidate = false;
            }

            if (!String.IsNullOrEmpty(createCIFModel.MotherMaidenName) && this.SubValidateIsNotSymbol(createCIFModel.MotherMaidenName.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Mother Maiden Name cannot contain symbol in first character!";
                isValidate = false;
            }

            if (this.SubValidateCannotContainsTitle(createCIFModel.MotherMaidenName))
            {
                strError += Environment.NewLine + "Mother Maiden Name cannot contain title before or title after!";
                isValidate = false;
            }

            if (this.SubValidateExceptionMotherName(createCIFModel.MotherMaidenName))
            {
                strError += Environment.NewLine + "Mother Maiden Name cannot contain 'kata tolakan parameter'!";
                isValidate = false;
            }

            if (!String.IsNullOrEmpty(createCIFModel.BirthPlace) && this.SubValidateFirstIsNotSpace(createCIFModel.BirthPlace.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Birth Place cannot contain space in first character!";
                isValidate = false;
            }

            if (this.SubValidateIsNotNumber(createCIFModel.BirthPlace))
            {
                strError += Environment.NewLine + "Birth Place cannot contain number!";
                isValidate = false;
            }

            if (!String.IsNullOrEmpty(createCIFModel.IDNumber) && this.SubValidateFirstIsNotSpace(createCIFModel.IDNumber.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Number Of Identity cannot contain space in first character!";
                isValidate = false;
            }

            if (!String.IsNullOrEmpty(createCIFModel.IDNumber) && this.SubValidateIsNotSymbol(createCIFModel.IDNumber.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Number Of Identity cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.IDTypeCode == "KTP" || createCIFModel.IDTypeCode == "SIM")
            {
                try
                {
                    long num = long.Parse(createCIFModel.IDNumber.Replace(".", "").Replace(",", "").Replace("-", "").Replace("_", "").Replace(" ", ""));
                }
                catch
                {
                    strError += Environment.NewLine + "Number Of Identity must number!";
                    isValidate = false;
                }
            }

            if (!String.IsNullOrEmpty(createCIFModel.Addressline1) && this.SubValidateFirstIsNotSpace(createCIFModel.Addressline1.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Address Line 1 cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.Addressline1.Length > 0 &&  this.SubValidateIsNotSymbol(createCIFModel.Addressline1.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Address Line 1 cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.Addressline2.Length > 0 &&  this.SubValidateFirstIsNotSpace(createCIFModel.Addressline2.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Address Line 2 cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.Addressline2.Length > 0 && this.SubValidateIsNotSymbol(createCIFModel.Addressline2.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Address Line 2 cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.Addressline3.Length > 0 && this.SubValidateFirstIsNotSpace(createCIFModel.Addressline3.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Address Line 3 cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.Addressline3.Length > 0 && this.SubValidateIsNotSymbol(createCIFModel.Addressline3.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Address Line 3 cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.Postalcode.Length > 0 && this.SubValidateFirstIsNotSpace(createCIFModel.Postalcode.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Postal Code cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.Postalcode.Length > 0 && this.SubValidateIsNotSymbol(createCIFModel.Postalcode.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Postal Code cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.Kelurahan.Length > 0 && this.SubValidateFirstIsNotSpace(createCIFModel.Kelurahan.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Kelurahan cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.Kelurahan.Length > 0 && this.SubValidateIsNotSymbol(createCIFModel.Kelurahan.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Kelurahan cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.Kecamatan.Length > 0 && this.SubValidateFirstIsNotSpace(createCIFModel.Kecamatan.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Kecamatan cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.Kecamatan.Length > 0 && this.SubValidateIsNotSymbol(createCIFModel.Kecamatan.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Kecamatan cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.DatiII.Length > 0 && this.SubValidateFirstIsNotSpace(createCIFModel.DatiII.Substring(0, 1)))
            {
                strError += Environment.NewLine + "DatiII cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.DatiII.Length > 0 && this.SubValidateIsNotSymbol(createCIFModel.DatiII.Substring(0, 1)))
            {
                strError += Environment.NewLine + "DatiII cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.Provinsi.Length > 0 && this.SubValidateFirstIsNotSpace(createCIFModel.Provinsi.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Provinsi cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.Provinsi.Length > 0 && this.SubValidateIsNotSymbol(createCIFModel.Provinsi.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Provinsi cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.IdIssuedDateDMY.Length > 1 && DateTimeLib.GetDateTime(createCIFModel.IdIssuedDate,"yyyy-MM-dd").Year > 1900)
            {
                if (this.SubValidateGreaterDate(DateTimeLib.GetDateTime(createCIFModel.IdIssuedDate, "yyyy-MM-dd")))
                {
                    strError += Environment.NewLine + "Identity Issued Date cannot be greater than today's date!";
                    isValidate = false;
                }
            }
            else
            {
                dsOutValidate = new local.APIMessage<DataSet>();
                dsOutValidate = _service.SubGetParameterIDType(createCIFModel.IDTypeCode);
                if (dsOutValidate.IsSuccess)
                {
                    if (dsOutValidate.Data != null && dsOutValidate.Data.Tables.Count > 0 && dsOutValidate.Data.Tables[0].Rows.Count == 0)
                    {
                        if (dsOutValidate.Data.Tables[0].Rows[0]["CFIDIX"].ToString().Trim().Equals("Y"))
                        {
                            strError += Environment.NewLine + "Identity Issued Date cannot be blank!";
                            isValidate = false;
                        }
                    }
                }
            }

            if (createCIFModel.DateIDExpiry != "" && DateTime.Parse(createCIFModel.DateIDExpiry).Year >= 1900)
            {
                if (this.SubValidateSmallerDate(DateTime.Parse(createCIFModel.DateIDExpiry)))
                {
                    strError += Environment.NewLine + "Identity Expired Date cannot be smaller than today's date!";
                    isValidate = false;
                }
            }
            else
            {
                dsOutValidate = new local.APIMessage<DataSet>();
                dsOutValidate = _service.SubGetParameterIDType(createCIFModel.IDTypeCode);
                if (dsOutValidate.IsSuccess)
                {
                    if (dsOutValidate.Data != null && dsOutValidate.Data.Tables.Count > 0 && dsOutValidate.Data.Tables[0].Rows.Count == 0)
                    {
                        if (dsOutValidate.Data.Tables[0].Rows[0]["CFIDDX"].ToString().Trim().Equals("Y"))
                        {
                            strError += Environment.NewLine + "Identity Expired Date cannot be blank!";
                            isValidate = false;
                        }
                    }
                }
            }

            //tiefanny
            if (createCIFModel.IDTypeCode.Equals("KTP"))
            {
                if (createCIFModel.IDNumber.Replace(".", "").Replace(",", "").Replace("-", "").Replace("_", "").Replace(" ", "").Length < 16)
                {
                    strError += Environment.NewLine + "Nomer KTP harus 16 digit!";
                    isValidate = false;
                }
            }
            else if (createCIFModel.IDTypeCode.Equals("NPWP"))
            {
                if (createCIFModel.IDNumber.Equals("12.345.678.91.100.00") || createCIFModel.IDNumber.Equals("00.000.000.00.000.00"))
                {
                    strError += Environment.NewLine + "Number Of Identity invalid value!";
                    isValidate = false;
                }

                if (this.SubValidateFormatNPWP(createCIFModel.IDNumber))
                {
                    strError += Environment.NewLine + "Number Of Identity NPWP format must like xx.xxx.xxx.x.xxx.xxx and number!";
                    isValidate = false;
                }
            }

            if (createCIFModel.FatcaFlag == "Y")
            {
                if (DateTime.Parse(createCIFModel.FatcaDate).Year >= 1900)
                {
                    if (this.SubValidateGreaterDate(DateTime.Parse(createCIFModel.FatcaDate)))
                    {
                        strError += Environment.NewLine + "Fatca Date cannot be greater than today's date!";
                        isValidate = false;
                    }
                }
            }

            //tiefanny
            if (createCIFModel.CustomerTypeCode.Trim() == "A")
            {             
                if (createCIFModel.IDTypeCode != "KTP" && createCIFModel.IDTypeCode != "SIM" &&
                    createCIFModel.IDTypeCode != "PP" && createCIFModel.IDTypeCode != "KP")
                {
                    strError += Environment.NewLine + "Untuk Nasabah Perorangan ID Utama yang diwajibkan adalah KTP / SIM / Passport / Kartu Pelajar";
                    isValidate = false;
                }

                if (!String.IsNullOrEmpty(createCIFModel.CountryOfCitizenship))
                {
                    if (createCIFModel.IDTypeCode == "PP" && createCIFModel.CountryOfCitizenship == "000")
                    {
                        strError += Environment.NewLine + "Untuk Nasabah Perorangan ID Utama Passport tidak boleh Kewarganegaraan Indonesia";
                        isValidate = false;
                    }

                    if (createCIFModel.IDTypeCode != "PP" && createCIFModel.CountryOfCitizenship != "000")
                    {
                        strError += Environment.NewLine + "Untuk Nasabah Perorangan Kewarganegaraan WNA harus ID Utama Passport";
                        isValidate = false;
                    }
                }

                if (createCIFModel.IDTypeCode == "KP")
                {
                    bool blnUmur = _service.SubCheckBirthDateSeventeen(createCIFModel.BirthDate, createCIFModel.CustomerTypeCode);
                    if (!blnUmur)
                    {
                        strError += Environment.NewLine + "Umur Nasabah >=17, Kartu Pelajar sebagai ID Utama tidak dapat digunakan";
                        isValidate = false;
                    }
                }
            }
            
            local.APIMessage<DataSet> dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.BranchNumber))
            {
                dsOut = _service.SubGetParameterBranchNumber(createCIFModel.BranchNumber.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Branch Number tidak ditemukan diparameter!";
                }
                
                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Branch Number tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.CustomerTypeCode))
            {
                dsOut = _service.SubGetParameterCustomerType(createCIFModel.CustomerTypeCode.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Customer Type Code tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Customer Type Code tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.TitleBeforeName))
            {
                dsOut = _service.SubGetParameterGelarSebelum(createCIFModel.TitleBeforeName.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Title Before Name tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Title Before Name tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.TitleAfterName))
            {
                dsOut = _service.SubGetParameterGelarSesudah(createCIFModel.TitleAfterName.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Title After Name tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Title After Name tidak ditemukan diparameter!";
                }
            }

            if (!String.IsNullOrEmpty(createCIFModel.Sexcode))
            {
                if (createCIFModel.Sexcode != "M" && createCIFModel.Sexcode != "F")
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Sex code must M or F!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.IDTypeCode))
            {
                dsOut = _service.SubGetParameterIDType(createCIFModel.IDTypeCode.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value ID Type Code tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value ID Type Code tidak ditemukan diparameter!";
                }
            }
            
            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.CountryOfCitizenship))
            {
                dsOut = _service.SubGetParameterMasterCountry(createCIFModel.CountryOfCitizenship.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Negara Kewarganegaraan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Negara Kewarganegaraan tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.CountryOfResidence))
            {
                dsOut = _service.SubGetParameterMasterCountry(createCIFModel.CountryOfResidence.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Negara Domisili tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Negara Domisili tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.StatusPernikahan))
            {
                dsOut = _service.SubGetParameterUserDefine(createCIFModel.CustomerTypeCode.ToString(),"2");

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Status Pernikahan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Status Pernikahan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                {
                    DataRow[] foundRows = dsOut.Data.Tables[0].Select("Kode like '" + createCIFModel.StatusPernikahan + "'");
                    if (foundRows.Length <= 0)
                    {
                        isValidate = false;
                        strError += Environment.NewLine + "Value Status Pernikahan tidak ditemukan diparameter!";
                    }
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.LevelPendidikan))
            {
                dsOut = _service.SubGetParameterUserDefine(createCIFModel.CustomerTypeCode.ToString(), "3");

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Level Pendidikan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Level Pendidikan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                {
                    DataRow[] foundRows = dsOut.Data.Tables[0].Select("Kode like '" + createCIFModel.LevelPendidikan + "'");
                    if (foundRows.Length <= 0)
                    {
                        isValidate = false;
                        strError += Environment.NewLine + "Value Level Pendidikan tidak ditemukan diparameter!";
                    }
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.RatarataTransaksi))
            {
                dsOut = _service.SubGetParameterUserDefine(createCIFModel.CustomerTypeCode.ToString(), "7");

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Rata-rata Transaksi tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Rata-rata Transaksi tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                {
                    DataRow[] foundRows = dsOut.Data.Tables[0].Select("Kode like '" + createCIFModel.RatarataTransaksi + "'");
                    if (foundRows.Length <= 0)
                    {
                        isValidate = false;
                        strError += Environment.NewLine + "Value Rata-rata Transaksi tidak ditemukan diparameter!";
                    }
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.RiskProfile))
            {
                dsOut = _service.SubGetParameterUserDefine(createCIFModel.RiskProfile.ToString(), "8");

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Risk Profile tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Risk Profile tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                {
                    DataRow[] foundRows = dsOut.Data.Tables[0].Select("Kode like '" + createCIFModel.RiskProfile + "'");
                    if (foundRows.Length <= 0)
                    {
                        isValidate = false;
                        strError += Environment.NewLine + "Value Risk Profile tidak ditemukan diparameter!";
                    }
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.Religion))
            {
                dsOut = _service.SubGetParameterReligion(createCIFModel.Religion.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Agama tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Agama tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.KodeLaporanTransaksiTunai))
            {
                dsOut = _service.SubGetParameterMasterLapTrxTunai(createCIFModel.KodeLaporanTransaksiTunai.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Kode Laporan Transaksi Tunai tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Kode Laporan Transaksi Tunai tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.RiskCode))
            {
                dsOut = _service.SubGetParameterIdentifikasiResiko(createCIFModel.RiskCode.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Risk Code tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Risk Code tidak ditemukan diparameter!";
                }
            }

            //validasi sql injection
            if (!SubValidateSQLInjection(createCIFModel, out tmp))
            {
                strError += Environment.NewLine + tmp;
                isValidate = false;
            }


            msgValidate.IsSuccess = isValidate;
            msgValidate.Data = "";
            msgValidate.Description = strError;

            return msgValidate;
        }

        public local.APIMessage<string> ValidateMaintenanceCIF(GeneralCIFModel createCIFModel)
        {
            _service.doLogging(API_LOGENVELOPModel.INFO, "Executing : " + _service.GetMethodName(), "", null, null, createCIFModel.GUID, createCIFModel.NIK, createCIFModel.Module, createCIFModel.Branch);

            bool isValidate = true;
            string strError = "";
            string tmp = "";
            local.APIMessage<string> msgValidate = new local.APIMessage<string>();
            
            //NIK RM tidak mandatory di API ini

            if (String.IsNullOrEmpty(createCIFModel.BranchNumber.Trim()))
            {
                strError += Environment.NewLine + "Branch Number cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.CustomerTypeCode.Trim()))
            {
                strError += Environment.NewLine + "Customer Type Code cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.CustomerName1.Trim()))
            {
                strError += Environment.NewLine + "Customer Name 1 cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.BirthPlace.Trim()))
            {
                strError += Environment.NewLine + "Birth Place cannot be empty!";
                isValidate = false;
            }

            if (createCIFModel.BirthDate.Year <= 1900)
            {
                strError += Environment.NewLine + "Birth Date cannot be empty!";
                isValidate = false;
            }
            else
            {
                if (this.SubValidateGreaterDate(createCIFModel.BirthDate))
                {
                    strError += Environment.NewLine + "Birth Date cannot be greater than today's date!";
                    isValidate = false;
                }
            }

            if (createCIFModel.CustomerTypeCode.Trim() == "A")
            {
                if (String.IsNullOrEmpty(createCIFModel.MotherMaidenName.Trim()))
                {
                    strError += Environment.NewLine + "Mother Mainden Name cannot be empty!";
                    isValidate = false;
                }

                if (!this.SubValidasiKarakterASCIIExtended(createCIFModel.MotherMaidenName, "Mother Mainden Name", out tmp))
                {
                    strError += Environment.NewLine + tmp;
                    isValidate = false;
                }
            }

            if (createCIFModel.CustomerTypeCode.Trim() != "A")
            {
                if (String.IsNullOrEmpty(createCIFModel.IDIssuePlace.Trim()))
                {
                    strError += Environment.NewLine + "ID Issued Place cannot be empty!";
                    isValidate = false;
                }
            }

            if (String.IsNullOrEmpty(createCIFModel.IDTypeCode.Trim()))
            {
                strError += Environment.NewLine + "Type Of Identity cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.IDNumber.Trim()))
            {
                strError += Environment.NewLine + "Number Of Identity cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.CountryOfCitizenship.Trim()))
            {
                strError += Environment.NewLine + "Country Of Citizenship cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.CountryOfResidence.Trim()))
            {
                strError += Environment.NewLine + "Country Of Resident cannot be empty!";
                isValidate = false;
            }

            if (createCIFModel.CustomerTypeCode.Trim() == "A")
            {
                if (String.IsNullOrEmpty(createCIFModel.StatusPernikahan.Trim()))
                {
                    strError += Environment.NewLine + "Marital Status cannot be empty!";
                    isValidate = false;
                }

                if (String.IsNullOrEmpty(createCIFModel.Religion.Trim()))
                {
                    strError += Environment.NewLine + "Religion cannot be empty!";
                    isValidate = false;
                }
                else
                {
                    if (createCIFModel.Religion.Trim().Equals("N/A"))
                    {
                        strError += Environment.NewLine + "Value Religion cannot N/A!";
                        isValidate = false;
                    }
                }

                if (String.IsNullOrEmpty(createCIFModel.LevelPendidikan.Trim()))
                {
                    strError += Environment.NewLine + "Level Of Education cannot be empty!";
                    isValidate = false;
                }
            }

            if (String.IsNullOrEmpty(createCIFModel.RatarataTransaksi.Trim()))
            {
                strError += Environment.NewLine + "Average transaction cannot be empty!";
                isValidate = false;
            }

            if (createCIFModel.CustomerTypeCode.Trim() != "A")
            {
                if (String.IsNullOrEmpty(createCIFModel.KodeLaporanTransaksiTunai.Trim()))
                {
                    strError += Environment.NewLine + "Cash Transaction Report Code cannot be empty!";
                    isValidate = false;
                }

                if (String.IsNullOrEmpty(createCIFModel.RiskCode.Trim()))
                {
                    strError += Environment.NewLine + "Identification Risk Code cannot be empty!";
                    isValidate = false;
                }
                else
                {
                    if (createCIFModel.RiskCode.Trim().Equals("27") && createCIFModel.RiskDescription.Trim().Length < 5)
                    {
                        strError += Environment.NewLine + "Business Field Information is at least filled with 5 characters!";
                        isValidate = false;
                    }
                }
            }

            if (String.IsNullOrEmpty(createCIFModel.TaxCode.Trim()))
            {
                strError += Environment.NewLine + "Tax Code cannot be empty!";
                isValidate = false;
            }

            if (String.IsNullOrEmpty(createCIFModel.FatcaFlag.Trim()))
            {
                strError += Environment.NewLine + "Flag Fatca cannot be empty!";
                isValidate = false;
            }
            else
            {
                if (createCIFModel.FatcaFlag.Trim().Equals("Y"))
                {
                    if (String.IsNullOrEmpty(createCIFModel.FatcaDate.Trim()))
                    {
                        strError += Environment.NewLine + "Fatca Date cannot be empty!";
                        isValidate = false;
                    }
                    else
                    {
                        if (this.SubValidateGreaterDate(Convert.ToDateTime(createCIFModel.FatcaDate)))
                        {
                            strError += Environment.NewLine + "Fatca Date cannot be greater than today's date!";
                            isValidate = false;
                        }
                    }
                }
            }

            if (createCIFModel.CRSFlag.Trim().Equals("Y"))
            {
                if (String.IsNullOrEmpty(createCIFModel.CRSDate.Trim()))
                {
                    strError += Environment.NewLine + "CRS Date cannot be empty!";
                    isValidate = false;
                }
                else
                {
                    if (this.SubValidateGreaterDate(Convert.ToDateTime(createCIFModel.CRSDate)))
                    {
                        strError += Environment.NewLine + "CRS Date cannot be greater than today's date!";
                        isValidate = false;
                    }
                }
            }

            if (this.SubValidateFirstIsNotSpace(createCIFModel.MotherMaidenName))
            {
                strError += Environment.NewLine + "Mother Maiden Name cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.MotherMaidenName.Length > 0 && this.SubValidateIsNotSymbol(createCIFModel.MotherMaidenName.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Mother Maiden Name cannot contain symbol in first character!";
                isValidate = false;
            }

            if (this.SubValidateExceptionMotherName(createCIFModel.MotherMaidenName))
            {
                strError += Environment.NewLine + "Mother Maiden Name cannot contain 'kata tolakan parameter'!";
                isValidate = false;
            }

            if (createCIFModel.BirthPlace.Length > 0 && this.SubValidateFirstIsNotSpace(createCIFModel.BirthPlace.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Birth Place cannot contain space in first character!";
                isValidate = false;
            }

            if (this.SubValidateIsNotNumber(createCIFModel.BirthPlace))
            {
                strError += Environment.NewLine + "Birth Place cannot contain number!";
                isValidate = false;
            }

            if (createCIFModel.IDIssuePlace.Length > 0 && this.SubValidateFirstIsNotSpace(createCIFModel.IDIssuePlace.Substring(0, 1)))
            {
                strError += Environment.NewLine + "ID Issued Place cannot contain space in first character!";
                isValidate = false;
            }

            if (this.SubValidateIsNotSymbolWithoutSpace(createCIFModel.IDIssuePlace))
            {
                strError += Environment.NewLine + "ID Issued Place cannot contain symbols!";
                isValidate = false;
            }

            if (createCIFModel.IDNumber.Length > 0 && this.SubValidateFirstIsNotSpace(createCIFModel.IDNumber.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Number Of Identity cannot contain space in first character!";
                isValidate = false;
            }

            if (createCIFModel.IDNumber.Length > 0 && this.SubValidateIsNotSymbol(createCIFModel.IDNumber.Substring(0, 1)))
            {
                strError += Environment.NewLine + "Number Of Identity cannot contain symbol in first character!";
                isValidate = false;
            }

            if (createCIFModel.BirthDate.Year <= 1900)
            {
                strError += Environment.NewLine + "Birth Date cannot be empty!";
                isValidate = false;
            }
            else
            {
                if (this.SubValidateGreaterDate(createCIFModel.BirthDate))
                {
                    strError += Environment.NewLine + "Birth Date cannot be greater than today's date!";
                    isValidate = false;
                }
            }

            local.APIMessage<DataSet> dsOutValidate = new local.APIMessage<DataSet>();
            dsOutValidate = _service.SubCheckIDTypeWithCustomerType(createCIFModel.CustomerTypeCode, createCIFModel.IDTypeCode);
            if (dsOutValidate.IsSuccess)
            {
                if (dsOutValidate.Data != null && dsOutValidate.Data.Tables.Count > 0 && dsOutValidate.Data.Tables[0].Rows.Count == 0)
                {
                    strError += Environment.NewLine + "Combinations Customer Type Code and Type Of Identity are not found!";
                    isValidate = false;
                }
            }
            else
            {
                strError += Environment.NewLine + dsOutValidate.Description;
                isValidate = false;
            }

            if (createCIFModel.IdIssuedDateDMY.Length > 1 && DateTimeLib.GetDateTime(createCIFModel.IdIssuedDate, "yyyy-MM-dd").Year >= 1900)
            {
                if (this.SubValidateGreaterDate(DateTimeLib.GetDateTime(createCIFModel.IdIssuedDate, "yyyy-MM-dd")))
                {
                    strError += Environment.NewLine + "Identity Issued Date cannot be greater than today's date!";
                    isValidate = false;
                }
            }
            else
            {
                dsOutValidate = new local.APIMessage<DataSet>();
                dsOutValidate = _service.SubGetParameterIDType(createCIFModel.IDTypeCode);
                if (dsOutValidate.IsSuccess)
                {
                    if (dsOutValidate.Data != null && dsOutValidate.Data.Tables.Count > 0 && dsOutValidate.Data.Tables[0].Rows.Count == 0)
                    {
                        if (dsOutValidate.Data.Tables[0].Rows[0]["CFIDIX"].ToString().Trim().Equals("Y"))
                        {
                            strError += Environment.NewLine + "Identity Issued Date cannot be blank!";
                            isValidate = false;
                        }
                    }
                }
            }

            if (createCIFModel.DateIDExpiry != "" && DateTime.Parse(createCIFModel.DateIDExpiry).Year >= 1900)
            {
                if (this.SubValidateSmallerDate(DateTime.Parse(createCIFModel.DateIDExpiry)))
                {
                    strError += Environment.NewLine + "Identity Expired Date cannot be smaller than today's date!";
                    isValidate = false;
                }
            }
            else
            {
                dsOutValidate = new local.APIMessage<DataSet>();
                dsOutValidate = _service.SubGetParameterIDType(createCIFModel.IDTypeCode);
                if (dsOutValidate.IsSuccess)
                {
                    if (dsOutValidate.Data != null && dsOutValidate.Data.Tables.Count > 0 && dsOutValidate.Data.Tables[0].Rows.Count == 0)
                    {
                        if (dsOutValidate.Data.Tables[0].Rows[0]["CFIDDX"].ToString().Trim().Equals("Y"))
                        {
                            strError += Environment.NewLine + "Identity Expired Date cannot be blank!";
                            isValidate = false;
                        }
                    }
                }
            }

            //tiefanny
            if (createCIFModel.IDTypeCode.Equals("KTP"))
            {
                if (createCIFModel.IDNumber.Replace(".", "").Replace(",", "").Replace("-", "").Replace("_", "").Replace(" ", "").Length < 16)
                {
                    strError += Environment.NewLine + "Nomer KTP harus 16 digit!";
                    isValidate = false;
                }
            }
            else if (createCIFModel.IDTypeCode.Equals("NPWP"))
            {
                if (createCIFModel.IDNumber.Equals("12.345.678.91.100.00") || createCIFModel.IDNumber.Equals("00.000.000.00.000.00"))
                {
                    strError += Environment.NewLine + "Number Of Identity invalid value!";
                    isValidate = false;
                }

                if (this.SubValidateFormatNPWP(createCIFModel.IDNumber))
                {
                    strError += Environment.NewLine + "Number Of Identity NPWP format must like xx.xxx.xxx.x.xxx.xxx and number!";
                    isValidate = false;
                }
            }

            if (createCIFModel.IDTypeCode == "KTP" || createCIFModel.IDTypeCode == "SIM")
            {
                try
                {
                    long num = long.Parse(createCIFModel.IDNumber.Replace(".", "").Replace(",", "").Replace("-", "").Replace("_", "").Replace(" ", ""));
                }
                catch
                {
                    strError += Environment.NewLine + "Number Of Identity must number!";
                    isValidate = false;
                }
            }

            tmp = "";
            local.APIMessage<DataSet> dsOut = new local.APIMessage<DataSet>();

            if (createCIFModel.EmployeeIndicator == "Y")
            {
                dsOut = _service.SubGetParameterMasterNamaKaryawan(createCIFModel.EmployeeNIK);
                if (dsOut.IsSuccess == true && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                {
                    tmp = dsOut.Data.Tables[0].Rows[0]["CFEMEN"].ToString().Trim();

                    if (createCIFModel.CustomerName1 != tmp.ToString().ToUpper().Trim())
                    {
                        strError += Environment.NewLine + "Nama Nasabah dan nama karyawan (di parameter) tidak sama, Hubungi HC!";
                        isValidate = false;
                    }
                }
            }
            else
            {
                createCIFModel.EmployeeNIK = "";
                createCIFModel.EmployeeName = "";
            }

            if (createCIFModel.CustomerTypeCode.Trim() == "A")
            {
                if (createCIFModel.IDTypeCode != "KTP" && createCIFModel.IDTypeCode != "SIM" &&
                    createCIFModel.IDTypeCode != "PP" && createCIFModel.IDTypeCode != "KP")
                {
                    strError += Environment.NewLine + "Untuk Nasabah Perorangan ID Utama yang diwajibkan adalah KTP / SIM / Passport / Kartu Pelajar";
                    isValidate = false;
                }

                if (!String.IsNullOrEmpty(createCIFModel.CountryOfCitizenship))
                {
                    if (createCIFModel.IDTypeCode == "PP" && createCIFModel.CountryOfCitizenship == "000")
                    {
                        strError += Environment.NewLine + "Untuk Nasabah Perorangan ID Utama Passport tidak boleh Kewarganegaraan Indonesia";
                        isValidate = false;
                    }

                    if (createCIFModel.IDTypeCode != "PP" && createCIFModel.CountryOfCitizenship != "000")
                    {
                        strError += Environment.NewLine + "Untuk Nasabah Perorangan Kewarganegaraan WNA harus ID Utama Passport";
                        isValidate = false;
                    }
                }

                if (createCIFModel.IDTypeCode == "KP")
                {
                    bool blnUmur = _service.SubCheckBirthDateSeventeen(createCIFModel.BirthDate, createCIFModel.CustomerTypeCode);
                    if (!blnUmur)
                    {
                        strError += Environment.NewLine + "Umur Nasabah >=17, Kartu Pelajar sebagai ID Utama tidak dapat digunakan";
                        isValidate = false;
                    }
                }
            }

            GeneralCIFModel modelOld = new GeneralCIFModel();
            modelOld = _service.InquiryDetailV2(createCIFModel).Data;
            if (modelOld != null)
            {
                if (!modelOld.CustomerTypeCode.Equals(createCIFModel.CustomerTypeCode))
                {
                    if (modelOld.CustomerTypeCode == "A" && createCIFModel.CustomerTypeCode != "A")
                    {
                        throw new Exception("Tidak bisa merubah Tipe Nasabah dari Perorangan menjadi Non Perorangan");
                    }

                    if (modelOld.CustomerTypeCode != "A" && createCIFModel.CustomerTypeCode == "A")
                    {
                        throw new Exception("Tidak bisa merubah Tipe Nasabah dari Non Perorangan menjadi Perorangan");
                    }
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.BranchNumber))
            {
                dsOut = _service.SubGetParameterBranchNumber(createCIFModel.BranchNumber.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Branch Number tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Branch Number tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.CustomerTypeCode))
            {
                dsOut = _service.SubGetParameterCustomerType(createCIFModel.CustomerTypeCode.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Customer Type Code tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Customer Type Code tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.TitleBeforeName))
            {
                dsOut = _service.SubGetParameterGelarSebelum(createCIFModel.TitleBeforeName.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Title Before Name tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Title Before Name tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.TitleAfterName))
            {
                dsOut = _service.SubGetParameterGelarSesudah(createCIFModel.TitleAfterName.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Title After Name tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Title After Name tidak ditemukan diparameter!";
                }
            }

            if (!String.IsNullOrEmpty(createCIFModel.Sexcode))
            {
                if (createCIFModel.Sexcode != "M" && createCIFModel.Sexcode != "F")
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Sex code must M or F!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.IDTypeCode))
            {
                dsOut = _service.SubGetParameterIDType(createCIFModel.IDTypeCode.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value ID Type Code tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value ID Type Code tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.CountryOfCitizenship))
            {
                dsOut = _service.SubGetParameterMasterCountry(createCIFModel.CountryOfCitizenship.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Negara Kewarganegaraan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Negara Kewarganegaraan tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.CountryOfResidence))
            {
                dsOut = _service.SubGetParameterMasterCountry(createCIFModel.CountryOfResidence.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Negara Domisili tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Negara Domisili tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.StatusPernikahan))
            {
                dsOut = _service.SubGetParameterUserDefine(createCIFModel.CustomerTypeCode.ToString(), "2");

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Status Pernikahan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Status Pernikahan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                {
                    DataRow[] foundRows = dsOut.Data.Tables[0].Select("Kode like '" + createCIFModel.StatusPernikahan + "'");
                    if (foundRows.Length <= 0)
                    {
                        isValidate = false;
                        strError += Environment.NewLine + "Value Status Pernikahan tidak ditemukan diparameter!";
                    }
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.LevelPendidikan))
            {
                dsOut = _service.SubGetParameterUserDefine(createCIFModel.CustomerTypeCode.ToString(), "3");

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Level Pendidikan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Level Pendidikan tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                {
                    DataRow[] foundRows = dsOut.Data.Tables[0].Select("Kode like '" + createCIFModel.LevelPendidikan + "'");
                    if (foundRows.Length <= 0)
                    {
                        isValidate = false;
                        strError += Environment.NewLine + "Value Level Pendidikan tidak ditemukan diparameter!";
                    }
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.RatarataTransaksi))
            {
                dsOut = _service.SubGetParameterUserDefine(createCIFModel.CustomerTypeCode.ToString(), "7");

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Rata-rata Transaksi tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Rata-rata Transaksi tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                {
                    DataRow[] foundRows = dsOut.Data.Tables[0].Select("Kode like '" + createCIFModel.RatarataTransaksi + "'");
                    if (foundRows.Length <= 0)
                    {
                        isValidate = false;
                        strError += Environment.NewLine + "Value Rata-rata Transaksi tidak ditemukan diparameter!";
                    }
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.RiskProfile))
            {
                dsOut = _service.SubGetParameterUserDefine(createCIFModel.RiskProfile.ToString(), "8");

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Risk Profile tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Risk Profile tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count > 0)
                {
                    DataRow[] foundRows = dsOut.Data.Tables[0].Select("Kode like '" + createCIFModel.RiskProfile + "'");
                    if (foundRows.Length <= 0)
                    {
                        isValidate = false;
                        strError += Environment.NewLine + "Value Risk Profile tidak ditemukan diparameter!";
                    }
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.Religion))
            {
                dsOut = _service.SubGetParameterReligion(createCIFModel.Religion.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Agama tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Agama tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.KodeLaporanTransaksiTunai))
            {
                dsOut = _service.SubGetParameterReligion(createCIFModel.KodeLaporanTransaksiTunai.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Kode Laporan Transaksi Tunai tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Kode Laporan Transaksi Tunai tidak ditemukan diparameter!";
                }
            }

            dsOut = new local.APIMessage<DataSet>();
            if (!String.IsNullOrEmpty(createCIFModel.RiskCode))
            {
                dsOut = _service.SubGetParameterIdentifikasiResiko(createCIFModel.RiskCode.ToString());

                if (dsOut == null && dsOut.Data == null)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Risk Code tidak ditemukan diparameter!";
                }

                if (dsOut.Data != null && dsOut.Data.Tables.Count > 0 && dsOut.Data.Tables[0].Rows.Count == 0)
                {
                    isValidate = false;
                    strError += Environment.NewLine + "Value Risk Code tidak ditemukan diparameter!";
                }
            }


            //validasi sql injection
            if (!SubValidateSQLInjection(createCIFModel, out tmp))
            {
                strError += Environment.NewLine + tmp;
                isValidate = false;
            }


            msgValidate.IsSuccess = isValidate;
            msgValidate.Data = "";
            msgValidate.Description = strError;

            return msgValidate;
        }

        #region internal library 
        private T MapSingleObject<T>(DataTable dt)
        {
            T t = Activator.CreateInstance<T>();
            if ((dt == null) || (dt.Rows.Count == 0))
            {
                t = default(T);
            }
            else
            {
                List<PropertyInfo> fields = new List<PropertyInfo>();
                fields.AddRange(typeof(T).GetProperties());

                for (int i = fields.Count - 1; i >= 0; i--)
                {
                    if (!dt.Columns.Contains(fields[i].Name))
                    {
                        fields.RemoveAt(i);
                    }
                }

                DataRow dr = dt.Rows[0];


                foreach (PropertyInfo fi in fields)
                {
                    object objectValue = System.Convert.ChangeType(dr[fi.Name], fi.PropertyType);

                    if (fi.PropertyType == typeof(System.String))
                    {
                        fi.SetValue(t, objectValue.ToString().Trim(), null);
                    }
                    else
                    {
                        fi.SetValue(t, objectValue, null);
                    }
                }
            }

            return t;
        }
        private List<T> MapListOfObject<T>(DataTable dt)
        {
            List<T> list = new List<T>();
            if ((dt == null) || (dt.Rows.Count == 0))
            {
                list = default(List<T>);
            }
            else
            {

                List<PropertyInfo> fields = new List<PropertyInfo>();
                fields.AddRange(typeof(T).GetProperties());

                for (int i = fields.Count - 1; i >= 0; i--)
                {
                    if (!dt.Columns.Contains(fields[i].Name))
                    {
                        fields.RemoveAt(i);
                    }
                }

                foreach (DataRow dr in dt.Rows)
                {
                    T t = Activator.CreateInstance<T>();
                    foreach (PropertyInfo fi in fields)
                    {
                        object objectValue = System.Convert.ChangeType(dr[fi.Name], fi.PropertyType);
                        if (fi.PropertyType == typeof(System.String))
                        {
                            fi.SetValue(t, objectValue.ToString().Trim(), null);
                        }
                        else
                        {
                            fi.SetValue(t, objectValue, null);
                        }
                    }

                    list.Add(t);
                }
            }
            return list;
        }
        private bool SubValidasiKarakterASCIIExtended(string paramValue, string paramObject, out string strError)
        {
            bool blnResult = false;
            strError = "";

            if (paramValue.Trim() == "")
            {
                return true;
            }
            try
            {
                System.Text.RegularExpressions.Match match =
                    System.Text.RegularExpressions.Regex.Match(paramValue
                    , @"[^a-zA-Z0-9~`!@#$%^&*()_+|\-=\\{}\[\]:"";'<>?,.\/\s]+");
                if (match.Success)
                {
                    blnResult = false;
                    strError = "There is a wrong character on " + paramObject + ", wrong character : " + match.Value.ToString();
                }
                else
                {
                    blnResult = true;
                }
            }
            catch (Exception ex)
            {
                blnResult = false;
                strError = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                _service.doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + _service.GetMethodName(), "", "", ex);
            }
            return blnResult;
        }
        private bool CheckForSQLInjection(string userInput)
        {
            bool isSQLInjection = false;

            string[] sqlCheckList = { "--",";--",";","/*","*/","@@","@","char","nchar","varchar","nvarchar","alter","begin","cast","create",
            "cursor","declare","delete","exec","execute","fetch","insert","sysobjects","syscolumns","table"};

            string CheckString = userInput.Replace("'", "''");

            for (int i = 0; i <= sqlCheckList.Length - 1; i++)
            {
                if ((CheckString.IndexOf(sqlCheckList[i], StringComparison.OrdinalIgnoreCase) >= 0))
                { isSQLInjection = true; }
            }
            return isSQLInjection;
        }
        private bool SubValidateIsNotNumber(string paramValue) //ProCIFCekData 5
        {
            bool blnResult = false;

            if (paramValue.Trim() == "")
            {
                return blnResult;
            }
            try
            {
                System.Text.RegularExpressions.Match match =
                    System.Text.RegularExpressions.Regex.Match(paramValue
                    , @"[0-9]+");
                blnResult = match.Success;
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private bool SubValidateIsNotSymbol(string paramValue) //ProCIFCekData 2,9
        {
            bool blnResult = false;

            if (paramValue.Trim() == "")
            {
                return blnResult;
            }
            try
            {
                System.Text.RegularExpressions.Match match =
                    System.Text.RegularExpressions.Regex.Match(paramValue
                    , @"[~`!@#$%^&*()_+|\-=\\{}\[\]:"";',.<>?\/\s]+");
                blnResult = match.Success;
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private bool SubValidateIsNotSymbolWithoutSpace(string paramValue) //ProCIFCekData 2,9
        {
            bool blnResult = false;

            if (paramValue.Trim() == "")
            {
                return blnResult;
            }
            try
            {
                System.Text.RegularExpressions.Match match =
                    System.Text.RegularExpressions.Regex.Match(paramValue
                    , @"[~`!@#$%^&*()_+|\-=\\{}\[\]:"";',.<>?\/]+");
                blnResult = match.Success;
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private bool SubValidateFirstIsNotSpace(string paramValue) //ProCIFCekData 1
        {
            bool blnResult = false;

            if (paramValue.Trim() == "")
            {
                return blnResult;
            }

            paramValue = paramValue.Substring(0, 1);
            try
            {
                System.Text.RegularExpressions.Match match =
                    System.Text.RegularExpressions.Regex.Match(paramValue
                    , @"[~ ]+");
                blnResult = match.Success;
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private bool SubValidateCannotContainsTitle(string paramValue) //ProCIFCekData 3,4,11
        {
            bool blnResult = false;
            if (paramValue.Trim() == "")
            {
                return blnResult;
            }
            paramValue = paramValue.ToLower();
            try
            {

                if (paramValue.StartsWith("rekening ") || //=> rekening %
                    paramValue.StartsWith("bsc ") ||
                    paramValue.StartsWith("m.eng ") ||
                    paramValue.StartsWith("m.pd ") ||
                    paramValue.StartsWith("msc ") ||
                    paramValue.StartsWith("msi ") ||
                    paramValue.StartsWith("mba ") ||
                    paramValue.StartsWith("mm ") ||
                    paramValue.StartsWith("m.sie ") ||
                    paramValue.StartsWith("mt ") ||
                    paramValue.StartsWith("shut ") ||
                    paramValue.StartsWith("skom ") ||
                    paramValue.StartsWith("spd ") ||
                    paramValue.StartsWith("spi ") ||
                    paramValue.StartsWith("spsi ") ||
                    paramValue.StartsWith("ssos ") ||
                    paramValue.StartsWith("se ") ||
                    paramValue.StartsWith("sei ") ||
                    paramValue.StartsWith("sh ") ||
                    paramValue.StartsWith("skm ") ||
                    paramValue.StartsWith("sp ") ||
                    paramValue.StartsWith("st ") ||
                    paramValue.StartsWith("dr. ") ||
                    paramValue.StartsWith("dr. ir. ") ||
                    paramValue.StartsWith("dra. ") ||
                    paramValue.StartsWith("drs. ") ||
                    paramValue.StartsWith("h. ") ||
                    paramValue.StartsWith("hj. ") ||
                    paramValue.StartsWith("ir. ") ||
                    paramValue.StartsWith("prof. dr. ir. ") ||
                    paramValue.StartsWith("prof. dr. ") ||
                    paramValue.StartsWith("dr. ") ||
                    paramValue.StartsWith("drg. ") ||
                    paramValue.StartsWith("drh. ") ||
                    paramValue.StartsWith("pt. ") ||
                    paramValue.StartsWith("cv. ") ||
                    paramValue.StartsWith("nv. ") ||
                    paramValue.EndsWith(" rekening") || //=> % rekening
                    paramValue.EndsWith(" bsc") ||
                    paramValue.EndsWith(" m.eng") ||
                    paramValue.EndsWith(" m.pd") ||
                    paramValue.EndsWith(" msc") ||
                    paramValue.EndsWith(" msi") ||
                    paramValue.EndsWith(" mba") ||
                    paramValue.EndsWith(" mm") ||
                    paramValue.EndsWith(" m.sie") ||
                    paramValue.EndsWith(" mt") ||
                    paramValue.EndsWith(" shut") ||
                    paramValue.EndsWith(" skom") ||
                    paramValue.EndsWith(" spd") ||
                    paramValue.EndsWith(" spi") ||
                    paramValue.EndsWith(" spsi") ||
                    paramValue.EndsWith(" ssos") ||
                    paramValue.EndsWith(" se") ||
                    paramValue.EndsWith(" sei") ||
                    paramValue.EndsWith(" sh") ||
                    paramValue.EndsWith(" skm") ||
                    paramValue.EndsWith(" sp") ||
                    paramValue.EndsWith(" st") ||
                    paramValue.EndsWith(" dr.") ||
                    paramValue.EndsWith(" dr. ir.") ||
                    paramValue.EndsWith(" dra.") ||
                    paramValue.EndsWith(" drs.") ||
                    paramValue.EndsWith(" h.") ||
                    paramValue.EndsWith(" hj.") ||
                    paramValue.EndsWith(" ir.") ||
                    paramValue.EndsWith(" prof. dr. ir.") ||
                    paramValue.EndsWith(" prof. dr.") ||
                    paramValue.EndsWith(" dr.") ||
                    paramValue.EndsWith(" drg.") ||
                    paramValue.EndsWith(" drh.") ||
                    paramValue.EndsWith(" pt.") ||
                    paramValue.EndsWith(" cv.") ||
                    paramValue.EndsWith(" nv."))

                {
                    blnResult = true;
                }
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private bool SubValidateExceptionMotherName(string paramValue) //ProCIFCekData 12,13,16
        {
            bool blnResult = false;

            if (paramValue.Trim() == "")
            {
                return blnResult;
            }

            try
            {
                local.APIMessage<DataSet> response = new local.APIMessage<DataSet>();
                response = _service.SubGetTolakanIbuKandung();

                if (response.IsSuccess)
                {
                    if (response.Data != null && response.Data.Tables.Count > 0 && response.Data.Tables[0].Rows.Count > 0)
                    {
                        DataRow[] foundRows = response.Data.Tables[0].Select("Value like '" + paramValue + "'");
                        if (foundRows.Length > 0)
                        {
                            blnResult = true;
                        }
                    }
                }

                if (paramValue.ToUpper().Contains("AAA") ||
                    paramValue.ToUpper().Contains("BBB") ||
                    paramValue.ToUpper().Contains("CCC") ||
                    paramValue.ToUpper().Contains("DDD") ||
                    paramValue.ToUpper().Contains("EEE") ||
                    paramValue.ToUpper().Contains("FFF") ||
                    paramValue.ToUpper().Contains("GGG") ||
                    paramValue.ToUpper().Contains("HHH") ||
                    paramValue.ToUpper().Contains("III") ||
                    paramValue.ToUpper().Contains("JJJ") ||
                    paramValue.ToUpper().Contains("KKK") ||
                    paramValue.ToUpper().Contains("LLL") ||
                    paramValue.ToUpper().Contains("MMM") ||
                    paramValue.ToUpper().Contains("NNN") ||
                    paramValue.ToUpper().Contains("OOO") ||
                    paramValue.ToUpper().Contains("PPP") ||
                    paramValue.ToUpper().Contains("QQQ") ||
                    paramValue.ToUpper().Contains("RRR") ||
                    paramValue.ToUpper().Contains("SSS") ||
                    paramValue.ToUpper().Contains("TTT") ||
                    paramValue.ToUpper().Contains("UUU") ||
                    paramValue.ToUpper().Contains("VVV") ||
                    paramValue.ToUpper().Contains("WWW") ||
                    paramValue.ToUpper().Contains("XXX") ||
                    paramValue.ToUpper().Contains("YYY") ||
                    paramValue.ToUpper().Contains("ZZZ"))
                {
                    blnResult = true;
                }
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private bool SubValidateGreaterDate(DateTime paramValue)
        {
            bool blnResult = false;
            try
            {
                if (paramValue > System.DateTime.Now.Date)
                {
                    blnResult = true;
                }
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private bool SubValidateSmallerDate(DateTime paramValue)
        {
            bool blnResult = false;
            try
            {
                if (paramValue < System.DateTime.Now.Date)
                {
                    blnResult = true;
                }
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private bool SubValidateFormatNPWP(string paramValue) //ProCIFCekData 2,9
        {
            bool blnResult = false;

            if (paramValue.Trim() == "")
            {
                return true;
            }
            try
            {
                System.Text.RegularExpressions.Match match =
                    System.Text.RegularExpressions.Regex.Match(paramValue
                    , @"^\d{2}.\d{3}.\d{3}.\d{1}.\d{3}.\d{3}$");
                blnResult = !match.Success;
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private bool SubValidateSQLInjection(object model, out string strError)
        {
            bool blnResult = true;
            strError = "";
            try
            {
                PropertyInfo[] properties = model.GetType().GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    if (property.PropertyType == typeof(System.String))
                    {
                        if (property.GetValue(model).ToString() != "")
                        {
                            if (CheckForSQLInjection(property.GetValue(model).ToString()))
                            {
                                strError = "Invalid value in property " + property.Name.ToString();
                                return false;
                            }
                        }
                    }
                }
            }
            catch
            {
                blnResult = false;
            }
            return blnResult;
        }
        private local.APIMessage<CIFCurrentDate> GetCurrentDate()
        {
            local.APIMessage<DataSet> msgParameter = new local.APIMessage<DataSet>();
            local.APIMessage<CIFCurrentDate> msgResponse = new local.APIMessage<CIFCurrentDate>();
            CIFCurrentDate currentDate = new CIFCurrentDate();
            try
            {
                msgParameter = _service.GetCurrentDate();
                msgResponse.IsSuccess = true;
                msgResponse.Description = "";
                msgResponse.Data = MapSingleObject<CIFCurrentDate>((DataTable)msgParameter.Data.Tables[0]);
            }
            catch (Exception ex)
            {
                msgResponse.Data = null;
                msgResponse.IsSuccess = false;
                //msgResponse.Description = ex.Message + Environment.NewLine + ex.StackTrace;
                msgResponse.Description = API_LOGENVELOPModel.CATCH_DESC_EXCEPTION;
                _service.doLogging(API_LOGENVELOPModel.ERROR, "Catch Exception : " + _service.GetMethodName(), "", "", ex);

            }

            return msgResponse;
        }
        #endregion

        #region DisposeObject
        // Flag: Has Dispose already been called?
        bool disposed = false;

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                //
            }
            disposed = true;
        }
        #endregion


    }
}
